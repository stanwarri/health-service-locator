;
(function($) {

    "use strict";

    var $body = $('body');
    var $head = $('head');
    // var $mainWrapper = $('#main-wrapper');

    $(document).ready(function() {

        /* -------------------------------------------------------------------------
    SEARCH NAV TOGGLE
  ------------------------------------------------------------------------- */
        var advanced_button = jQuery(".search-nav .advanced-search-button");
        var submenu = jQuery(".search-nav .sub-menu");

        $(advanced_button).on('click', function() {
            jQuery(this).parent('.search-nav').find('.sub-menu').slideToggle(500);
            if (jQuery(this).hasClass('active')) {
                jQuery(this).removeClass('active');
            } else {
                jQuery(this).addClass('active');
            }
        });

        submenu.find("li").click(function() {
            submenu.find("li").removeClass('active');
            jQuery(this).addClass('active');
            var getclass = jQuery(this).attr('class');
            jQuery(this).addClass('active');
            jQuery(".search-nav .advanced-search-button").val(getclass);
            var geticon = jQuery(this).find('i').attr('class');
            advanced_button.find('i').attr('class',geticon + ' fa-lg');
            advanced_button.removeClass('active');
            jQuery('.search-nav').find('ul').slideUp(500);
        });

        /* -------------------------------------------------------------------------
    MENU NAV
  ------------------------------------------------------------------------- */
        $('.nav').each(function() {

            var self = $(this);

            // HOVER SUBMENU
            self.find('li.has-submenu').hover(function() {
                $(this).addClass('hover');
                $(this).find('>.sub-menu').stop(true, true).fadeIn(200);
            }, function() {
                $(this).removeClass('hover');
                $(this).find('>.sub-menu').stop(true, true).delay(10).fadeOut(200);
            });
        });

        /* -------------------------------------------------------------------------
    HEADER TOGGLES
  ------------------------------------------------------------------------- */

        // SEARCH TOGGLE
        $( '.search-toggle' ).click(function(){
            if ( $( '.nav-wrapper-mobile' ).is( ':visible' ) ) {
                $( '.nav-wrapper-mobile' ).slideUp(300);
            }
            $( '.search-nav.mobile' ).slideToggle(300, function(){
                });
        });

        // NAVBAR TOGGLE
        $( '.navbar-toggle' ).click(function(){
            if ( $( '.search-nav.mobile' ).is( ':visible' ) ) {
                $( '.search-nav.mobile' ).slideUp(300);
            }
            $( '.nav-wrapper-mobile' ).slideToggle(300, function(){
                });
        });

        /* -------------------------------------------------------------------------
     HEADER MENU MOBILE
   ------------------------------------------------------------------------- */
        $('.nav-wrapper-mobile > .main-menu').each(function() {

            var self = $(this);

            // CREATE TOGGLE BUTTONS
            self.find( 'li.has-submenu' ).each(function(){
                $(this).append( '<button class="submenu-toggle"><i class="fa fa-chevron-down"></i></button>' );
            });

            // TOGGLE SUBMENU
            self.find('.submenu-toggle').each(function() {
                $(this).click(function() {
                    $(this).parent().find('> .sub-menu').slideToggle(200);
                    $(this).find('.fa').toggleClass('fa-chevron-up fa-chevron-down');
                });
            });

        });
        /* -------------------------------------------------------------------------
    MATCH HEIGHT (SECTION CATEGORIES)
  ------------------------------------------------------------------------- */
        $(function() {
            $('.list .category-box, .listing-box').matchHeight();
        });



        /* -------------------------------------------------------------------------
    TABS
  ------------------------------------------------------------------------- */
        $('.responsive-tabs').each(function() {
            $(this).responsiveTabs({
                accordionOn: ['xs'] // xs, sm, md, lg
            });
        });


        /* -------------------------------------------------------------------------
  MEDIA QUERY BREAKPOINT
  ------------------------------------------------------------------------- */
        var uouMediaQueryBreakpoint = function() {

            if ($('#media-query-breakpoint').length < 1) {
                $('body').append('<var id="media-query-breakpoint"><span></span></var>');
            }
            var value = $('#media-query-breakpoint').css('content');
            if (typeof value !== 'undefined') {
                value = value.replace("\"", "").replace("\"", "").replace("\'", "").replace("\'", "");
                if (isNaN(parseInt(value, 10))) {
                    $('#media-query-breakpoint span').each(function() {
                        value = window.getComputedStyle(this, ':before').content;
                    });
                    value = value.replace("\"", "").replace("\"", "").replace("\'", "").replace("\'", "");
                }
                if (isNaN(parseInt(value, 10))) {
                    value = 1199;
                }
            } else {
                value = 1199;
            }
            return value;

        };

        /* -------------------------------------------------------------------------
    SELECT BOX
  ------------------------------------------------------------------------- */
        $.fn.uouSelectBox = function() {

            var self = $(this),
            select = self.find('select');
            self.prepend('<ul class="select-clone custom-list"></ul>');

            var placeholder = select.data('placeholder') ? select.data('placeholder') : select.find('option:eq(0)').text(),
            clone = self.find('.select-clone');
            self.prepend('<input class="value-holder" type="text" disabled="disabled" placeholder="' + placeholder + '"><div class="advanced-select-button"><i class="fa fa-chevron-down"></i></div>');
            var value_holder = self.find('.value-holder');

            // INPUT PLACEHOLDER FIX FOR IE
            if ($.fn.placeholder) {
                self.find('input, textarea').placeholder();
            }

            // CREATE CLONE LIST
            select.find('option').each(function() {
                if ($(this).attr('value')) {
                    clone.append('<li data-value="' + $(this).val() + '">' + $(this).text() + '</li>');
                }
            });

            // CLICK TOGGLE
            self.click(function() {
                var media_query_breakpoint = uouMediaQueryBreakpoint();
                if (media_query_breakpoint > 991) {
                    clone.slideToggle(300);
                    self.toggleClass('active');
                    var searchButton = self.find('.advanced-select-button');
                    searchButton.toggleClass('active');
                    if(self.hasClass("active")){
                        searchButton.find(".fa").removeClass("fa-chevron-down");
                        searchButton.find(".fa").addClass("fa-chevron-up");
                    } else{
                        searchButton.find(".fa").addClass("fa-chevron-down");
                        searchButton.find(".fa").removeClass("fa-chevron-up");
                    }
                }
            });

            // CLICK
            clone.find('li').click(function() {
                select.change();
                value_holder.val($(this).text());
                select.find('option[value="' + $(this).attr('data-value') + '"]').attr('selected', 'selected');

                // IF LIST OF LINKS
                if (self.hasClass('links')) {
                    window.location.href = select.val();
                }

            });

            // HIDE LIST
            self.bind('clickoutside', function(event) {
                clone.slideUp(100);
            });

            // LIST OF LINKS
            if (self.hasClass('links')) {
                select.change(function() {
                    window.location.href = select.val();
                });
            }

        };

        /* -------------------------------------------------------------------------
    RADIO INPUT
  ------------------------------------------------------------------------- */
        $.fn.uouRadioInput = function(){

            var self = $(this),
            input = self.find( 'input' ),
            group = input.attr( 'name' );

            // INITIAL STATE
            if ( input.is( ':checked' ) ) {
                self.addClass( 'active' );
            }

            // CHANGE STATE
            input.change(function(){
                if ( group ) {
                    $( '.radio-input input[name="' + group + '"]' ).parent().removeClass( 'active' );
                }
                if ( input.is( ':checked' ) ) {
                    self.addClass( 'active' );
                }
            });
        };

        /* -------------------------------------------------------------------------
      BACK TO TOP BUTTON
  ------------------------------------------------------------------------- */
        $('#back-to-top').each(function () {

            var $this = $(this);

            $this.on('click', function (event) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: 0
                }, 300);
            });

            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    $this.fadeIn(200);
                } else if ($(this).scrollTop() < 250) {
                    $this.fadeOut(200);
                }
            });

        });

        // GET ACTUAL MEDIA QUERY BREAKPOINT
        var media_query_breakpoint = uouMediaQueryBreakpoint();

        // SELECT BOX
        $( '.select-box' ).each(function(){
            $(this).uouSelectBox();
        });

        // RADIO INPUT
        $( '.radio-input' ).each(function(){
            $(this).uouRadioInput();
        });

        $(window).resize(function(){
            if ( uouMediaQueryBreakpoint() !== media_query_breakpoint ) {
                media_query_breakpoint = uouMediaQueryBreakpoint();

                /* RESET HEADER ELEMENTS */
                $( '.search-nav.mobile, .nav-wrapper-mobile' ).removeAttr( 'style' );
            }
        });
  
  
        // responsive menu

        window.rmenu = function(){

            var	nav = $('nav[role="navigation"]'),
            header = $('[role="banner"]');

            var rMenu = new Object();

            rMenu.init = function(){
                rMenu.checkWindowSize();
                $(window).on('resize',rMenu.checkWindowSize);
            }

            rMenu.checkWindowSize = function(){

                if($(window).width() < 992){
                    rMenu.Activate();
                }
                else{
                    rMenu.Deactivate();
                }

            }
            // add click events
            rMenu.Activate = function(){
                if($('html').hasClass('md_touch')) header.off('.touch');
                header.off('click').on('click.responsivemenu','#menu_button',rMenu.openClose);
                header.on('click.responsivemenu','.main_menu li a',rMenu.openCloseSubMenu);
                nav.find('.touch_open_sub').removeClass('touch_open_sub').children('a').removeClass('prevented');
            }
            // remove click events
            rMenu.Deactivate = function(){
                header.off('.responsivemenu');
                nav.removeAttr('style').find('li').removeClass('current_click')
                .end().find('.sub_menu_wrap').removeAttr('style').end().find('.prevented').removeClass('prevented').end()
                if($('html').hasClass('md_touch')) header.off('click').on('click.touch','.main_menu li a',rMenu.touchOpenSubMenu);
            }

            rMenu.openClose = function(){

                $(this).toggleClass('active');
                nav.stop().slideToggle();

            }

            rMenu.openCloseSubMenu = function(e){

                var self = $(this);

                if(self.next('.sub_menu_wrap').length){
                    self.parent()
                    .addClass('current_click')
                    .siblings()
                    .removeClass('current_click')
                    .children(':not(span,a)')
                    .slideUp();
                    self.next().stop().slideToggle();
                    self.parent().siblings().children('a').removeClass('prevented');

                    if(!(self.hasClass('prevented'))){
                        e.preventDefault();
                        self.addClass('prevented');
                    }else{
                        self.removeClass('prevented');
                    }
                }

            }

            rMenu.touchOpenSubMenu = function(event){
                var self = $(this);

                if(self.next('.sub_menu_wrap').length){

                    if(!(self.hasClass('prevented'))){
                        event.preventDefault();
                        self.addClass('prevented');
                    }else{
                        self.removeClass('prevented');
                    }

                }
				
            }

            rMenu.init();
        }

        rmenu();
  
  
    });

    // Touch
    // ---------------------------------------------------------
    var dragging = false;

    $body.on('touchmove', function() {
        dragging = true;
    });

    $body.on('touchstart', function() {
        dragging = false;
    });



}(jQuery));
