@extends('layouts.admin')
@section('title', 'Manag Services')

@section('content')
<a data-toggle="modal" href="#add_equipment" class="btn btn-primary btn-sm pull-right" style="margin-top:5px; margin-right: 10px">Add New Equipment</a> 

<div class="module">
    <div class="module-head">
        <h3>Health Equipment</h3>
    </div>
    <div class="module-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Equipment name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($equipment as $tool)
                <tr>
                    <td>{{ $tool->name }}</td>
                    <td>
                        <a href="{{ route('admin.equipment.edit', [$tool->id])}}" class="btn btn-mini btn-primary">
                            <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        <a rel="{{$tool->id}}" class="delete_toggler btn btn-mini btn-danger">
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_equipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Equipment</h4>
            </div>
            <div class="modal-body">
                @if($errors->all())
                <div class="bs-callout bs-callout-danger">
                    <h4>{{ trans('admin.please_fix_errors') }}</h4>
                    {{ HTML::ul($errors->all())}}
                </div>
                @endif
                {{ Form::open(array('route' => 'admin.equipment.new', 'class'=>'form-horizontal row-fluid'))}}
                <div class="control-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls">
                        {{ Form::text('name',null,array('class'=>'span8', 'placeholder'=>'Equipment Name'))}}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                </div>
                {{ Form::close()}}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal -->
<div class="modal fade" id="delete_equipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Equipment</h4>
            </div>
            <div class="modal-body">
                <p class="lead text-center">Are you sure you want to delete this equipment</p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" href="#delete_equipment" class="btn btn-default">Keep</a>
                <a href="" id="delete_link" class="btn btn-danger">Delete</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
<script>    
    $(document).ready(function(){
        @if($errors->all())
            $('#add_equipment').modal('show');
        @endif

        // Populate the field with the right data for the modal when clicked
        $('.delete_toggler').each(function(index,elem) {
            $(elem).click(function(e){
                e.preventDefault();
                var href = "{{ route('admin.equipment.delete') }}/";
                $('#delete_link').attr('href',href + $(elem).attr('rel'));
                $('#delete_equipment').modal('show');
            });
        });
    });
</script>
@stop
