<?php
namespace Hsl\Facades;
use Illuminate\Support\Facades\Facade;



/**
 * Description of MailService
 *
 * @author Stanley Warri
 */
class MailService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'service.mail';
    }
}
