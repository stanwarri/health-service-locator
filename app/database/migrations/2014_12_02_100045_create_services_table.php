<?php

use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration {

    public function up()
    {
        Schema::create('services', function($table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('services');
    }

}
