<?php

namespace Hsl\Providers;

use Illuminate\Support\ServiceProvider;
/**
 * Description of MailServiceProvider
 *
 * @author Stanley Warri
 */
class MailServiceProvider extends ServiceProvider
{
    public function register() 
    {
        $this->app->bind(
            'Hsl\Services\Mail\MailServiceInterface',
            'Hsl\Services\Mail\DefaultMailService'
        );
        $this->app['service.mail'] = $this->app->share(function ($app) {
            return $this->app->make('Hsl\Services\Mail\MailServiceInterface');
        });
    }

}
