<?php

namespace Hsl\Repositories;

interface EquipmentRepositoryInterface
{
    /**
     * Get an array of key-value (id => name) pairs of all tags.
     *
     * @return array
     */
    public function listAll();

    /**
     * Find all tags.
     *
     * @param  string $orderColumn
     * @param  string $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|\Service[]
     */
    public function findAll($orderColumn = 'created_at', $orderDir = 'desc');

    /**
     * Find a tag by id.
     *
     * @param  mixed $id
     * @return Service
     */
    public function findById($id);
    /**
     * Create a new tag in the database.
     *
     * @param  array $data
     * @return Service
     */
    public function create(array $data);

    /**
     * Update the specified tag in the database.
     *
     * @param  mixed $id
     * @param  array $data
     * @return Service
     */
    public function update($id, array $data);

    /**
     * Delete the specified tag from the database.
     *
     * @param  mixed $id
     * @return void
     */
    public function delete($id);
}
