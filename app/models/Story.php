<?php

class Story extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stories';
    protected $fillable = [
        'fullname',
        'address',
        'email',
        'phone',
        'title',
        'status',
        'featured',
        'story',
        'slug',
        'featured_image'
    ];
    
       protected $dates = array('created_at');

}
