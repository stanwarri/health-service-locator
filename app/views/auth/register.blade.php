@extends('layouts.auth')
@section('title', 'Regsiter to Admincp Access')

@section('content')     
    <!-- start:form register -->
    <div class="container">
        {{ Form::open(array('route' => 'auth.register', 'class' => 'form-login')) }}
            <h2 class="form-login-heading">Admin Registration</h2>
            <div class="login-wrap">
                <input type="text" class="form-control" placeholder="Full Name" autofocus>
                <input type="text" class="form-control" placeholder="User Name" autofocus>
                <input type="text" class="form-control" placeholder="Email" autofocus>
                <input type="password" class="form-control" placeholder="Password">
                <input type="password" class="form-control" placeholder="Re-type Password">
                <button class="btn btn-lg btn-login btn-block" type="submit">Submit</button>
                <div class="registration">
                    Already Registered.
                    <a class="" href="{{ route('auth.login') }}">
                        Login
                    </a>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@stop
