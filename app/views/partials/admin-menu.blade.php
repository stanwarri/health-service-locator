<div class="sidebar">
    <ul class="widget widget-menu unstyled">
        <li class="@if(Request::segment(2) == ''){{ 'active' }} @endif">
            <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
        </li>
        <li>
            <a  href="{{ route('admin.story.index') }}"><i class="fa fa-newspaper-o fa-3x"></i> Health Stories</a>
        </li>
        <li class="@if(Request::segment(2) == 'hospitals'){{ 'active' }} @endif">
            <a href="{{ route('admin.hospitals.index') }}"><i class="fa fa-hospital-o fa-3x"></i> Hospitals</a>
        </li>
        <li>
            <a  href="{{ route('admin.cases.index') }}"><i class="fa fa-ambulance fa-3x"></i> Health Cases</a>
        </li>
        <li>
            <a  href="{{ route('admin.services.index') }}"><i class="fa fa-medkit fa-3x"></i> Health Services</a>
        </li>
        <li>
            <a  href="{{ route('admin.equipment.index') }}"><i class="fa fa-cutlery fa-3x"></i> Health Equipment</a>
        </li>
        <li>
            <a  href="{{ route('admin.emergency.index') }}"><i class="fa fa-phone fa-3x"></i> Emergency Numbers</a>
        </li>
<!--        <li><a class="collapsed" data-toggle="collapse" href="#users"><i class="fa fa-users fa-3x">
                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                </i> Users </a>
            <ul id="users" class="collapse unstyled">
                <li class="@if(Request::segment(2) == 'users'){{ 'active' }} @endif">
                    <a href="{{ route('admin.users.index') }}">List Users</a>
                </li>
                <li>
                    <a href="{{ route('admin.users.create') }}">Add New User</a>
                </li>
            </ul>
        </li>-->
    </ul>
</div>
<!--/.sidebar-->