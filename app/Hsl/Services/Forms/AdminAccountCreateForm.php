<?php

namespace Hsl\Services\Forms;

use Illuminate\Config\Repository;

class AdminAccountCreateForm extends AbstractForm
{

    protected $config;
    protected $rules = array(
        'fullname' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'confirmed|min:6',
        'password_confirmation' => 'same:password'
    );

    public function __construct(Repository $config)
    {
        parent::__construct();

        $this->config = $config;
    }

}
