<?php

namespace Hsl\Repositories;

use User;

interface UserRepositoryInterface
{

    public function findAll();

    public function findById($id);

    public function findByUsername($username);

    public function findByEmail($email);
    
    public function adminCreateuser(array $data);

    public function delete($id);

    public function findByActivationCode($code);
    
    public function setActivated($user);

    public function findByPasswordResetCode($code);

    public function setRecoverPasswordRequestState($user);

    public function setRecoverPasswordCompleteState($user, $data);

    public function updateSettings($user, array $data);

    public function getLoginForm();
    
    public function getAdminAccountCreateForm();

    public function getAccountCreateForm();

    public function getRecoverPasswordForm();

    public function getResetPasswordForm();

    public function getAccountSettingsForm();
}

?>
