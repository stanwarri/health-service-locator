<?php

use Illuminate\Database\Migrations\Migration;

class CreateHealthCasesTable extends Migration {

    public function up()
    {
        Schema::create('health_cases', function($table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('health_cases');
    }

}