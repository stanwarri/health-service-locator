<?php

class Survey extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'surveys';
    protected $fillable = [
        'fullname',
        'email',
        'health_case',
        'where_get_help',
        'rate_get_help',
        'explain_help',
        'explain_facility',
        'share_story',
        'photo'
    ];

}
