@extends('layouts.admin') 
@section('title', 'Dashboard')

@section('content')
<div class="btn-controls">
    <div class="btn-box-row row-fluid">
        <a href="{{ route('admin.story.index') }}" class="btn-box big span4"><i class="fa fa-newspaper-o fa-3x""></i><b>Health Stories</b>
        </a>
        <a href="{{ route('admin.hospitals.index') }}" class="btn-box big span4"><i class="fa fa-hospital-o fa-3x""></i><b>Hospitals</b>
        </a>
        <a href="{{ route('admin.cases.index') }}" class="btn-box big span4"><i class="fa fa-ambulance fa-3x"></i><b>Health Cases</b>
        </a>

    </div>
    <div class="btn-box-row row-fluid">
        <a href="{{ route('admin.services.index') }}" class="btn-box big span4"><i class="fa fa-medkit fa-3x"></i><b>Health Services</b>
        </a>
        <a href="{{ route('admin.emergency.index') }}" class="btn-box big span4"><i class="fa fa-phone fa-3x"></i><b>Emergency Numbers</b>
        </a>
<!--        <a href="{{ route('admin.users.index') }}" class="btn-box big span4"><i class="fa fa-users fa-3x"></i><b>Manage Users</b>
        </a>-->
    </div>
    <div class="btn-box-row row-fluid">

    </div>
</div>
@stop