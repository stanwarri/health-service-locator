<?php

namespace Hsl\Repositories;

use Hospital;

interface HospitalRepositoryInterface
{
    /**
     * Get an array of key-value (id => name) pairs of all tags.
     *
     * @return array
     */
    public function listAll();

    /**
     * Find all tags.
     *
     * @param  string $orderColumn
     * @param  string $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|\Hospital[]
     */
    public function findAll($orderColumn = 'created_at', $orderDir = 'desc');

    public function findById($id);
    
    //Find all hospitals in a state
    public function findByStateId($id);
    
    public function listCasesIdsForHospital(Hospital $hospital);
    
    public function listServicesIdsForHospital(Hospital $hospital);
    
    public function listEquipmentIdsForHospital(Hospital $hospital);
    /**
     * Create a new tag in the database.
     *
     * @param  array $data
     * @return Hospital
     */
    public function create(array $data);

    /**
     * Update the specified tag in the database.
     *
     * @param  mixed $id
     * @param  array $data
     * @return Hospital
     */
    public function update($id, array $data);

    /**
     * Delete the specified tag from the database.
     *
     * @param  mixed $id
     * @return void
     */
    public function delete($id);
    
    //Search for hospitals based on name
    public function searchByTermPaginated($term, $perPage);
}
