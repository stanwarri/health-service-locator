<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Health Service Locator - @yield('title')</title>
        <link type="text/css" href="{{ asset('assets/css/admin/bootstrap.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('assets/css/admin/bootstrap-responsive.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('assets/css/admin/style.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
        <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i>
                    </a>
                    <a class="brand" href="{{ route('auth.login') }}">
                        HSL Admin Login
                    </a>
                </div>
            </div><!-- /navbar-inner -->
        </div><!-- /navbar -->
        
            @yield('content')

        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 The Nation </b> All rights reserved.
            </div>
        </div>
        <script src="{{ asset('assets/js/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/admin/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/admin/bootstrap.min.js') }}" type="text/javascript"></script>
    </body>
    </html>