<?php

namespace Admin;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Hsl\Facades\MailService;
use Hsl\Repositories\UserRepositoryInterface;

class UserController extends \BaseController
{

    protected $users;

    public function __construct(UserRepositoryInterface $users)
    {
        parent::__construct();

        $this->users = $users;
    }

    public function index()
    {
        $users = $this->users->findAll();

        return View::make('admin.user.index', compact('users'));
    }

    public function create()
    {
        return View::make('admin.user.create');
    }

    public function store()
    {
        $accountCreateForm = $this->users->getAdminAccountCreateForm();
        if (!$accountCreateForm->isValid()) {
            return Redirect::route('admin.users.create')
                            ->withInput()
                            ->withErrors($accountCreateForm->getErrors());
        }

        $data = $accountCreateForm->getInputData();
        $data['username'] = str_random(6);

        $user = $this->users->adminCreateUser($data);

        if ($user) {
            $mail_from = Config::get('config.site_email');
            $site_name = Config::get('config.site_name');
            MailService::sendAdminAccountRegisterEmail($user->email, $mail_from, $site_name, URL::route('account.activate', $user->activation_code), $data['password'], $user->fullname);
            return Redirect::route('admin.users.index')
                            ->with('success', 'Activation email has been sent to the user');
        }
    }

    public function getEditAccount()
    {
        $user = Auth::user();
        return View::make('admin.user.account', [
                    'user' => $user
                ]);
    }

    public function postEditAccount()
    {
        $form = $this->users->getAccountSettingsForm();

        if (!$form->isValid()) {
            return $this->redirectBack([ 'errors' => $form->getErrors()]);
        }

        $this->users->updateAccountSettings(Auth::user(), Input::all());

        return $this->redirectRoute('admin.users.account.edit')->with('success', 'Account Settings updated successfully');
    }
    
     //Get user edit account profile

    public function editUser($id)
    {
        $user = $this->users->findById($id);
        return View::make('admin.user.edit', [
                    'user' => $user
                ]);
    }

    public function postEditUser($id)
    {
        $form = $this->users->getAccountSettingsForm();

        if (!$form->isValid()) {
            return $this->redirectBack([ 'errors' => $form->getErrors()]);
        }
        
        $user = $this->users->findById($id);
        
        $this->users->updateAccountSettings($user, Input::all());

        return $this->redirectRoute('admin.users.edit', $id)->with('success', 'Account Settings updated successfully');
    }

    public function delete($id)
    {
        $this->users->delete($id);

        return $this->redirectRoute('admin.users.index');
    }

}
