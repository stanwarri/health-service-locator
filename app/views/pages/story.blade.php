@extends('layouts.main')
@section('title', $story->title)

@section('content')
<!-- Start Main-Content -->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="page-content">
                <div class="col-md-8">
                    <h2>{{ $story->title }}</h2>
                    <img src="{{ asset('assets/uploads/'.$story->featured_image.'') }}" alt="">
                    <br/>
                    <ul class="social custom-list">
                        <li><a href="http://www.facebook.com/sharer.php?u={{ urlencode(Request::url()) }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://plus.google.com/share?url={{ urlencode(Request::url()) }}" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::url()) }}&text={{{ $story->title }}}&via=http://thenationonlineng.net" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                    {{ $story->story }}
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                var disqus_shortname = '{{ Config::get("config.disqus_shortname") }}';
                                var disqus_identifier = '{{ $story->id }}';

                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category-box">
                        <header class="category-header garden clearfix">
                            <a href="#">
                                <div class="category-icon"><i class="fa fa-medkit"></i></div>
                                <div class="category-title">
                                    <h5>Health Cases</h5>
                                </div>
                            </a>
                        </header>
                        <div class="panel-group health-cases" id="accordion">
                            @foreach($cases as $case)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $case->id }}">
                                            {{ $case->name }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{ $case->id }}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="category-list">
                                            <ul class="custom-list">
                                                <?php
                                                $facilities = 0;
                                                foreach ($case->hospitals as $hospital) {
                                                    if ($facilities == 6)
                                                        break;
                                                    echo '<li><a href="' . route('hospital.view', $hospital->id) . '">' . $hospital->name . '</a></li>';

                                                    $facilities++;
                                                }
                                                ?>
                                                <li><a href="{{ route('health.case', $case->slug) }}" class="text-center"><i class="fa fa-arrow-circle-right"></i> View All</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <!--  Emergency Numbers-->

                    <div class="category-box">
                        <header class="category-header garden clearfix">
                            <a href="#">
                                <div class="category-icon"><i class="fa fa-phone"></i></div>
                                <div class="category-title">
                                    <h5>Emergency Numbers</h5>
                                </div>
                            </a>
                        </header>
                        <div id="category-content-emergency">
                            @foreach($emergencies as $emergency)
                            <h5>{{ $emergency->name }}</h5>
                            <div class="category-list">
                                <ul class="custom-list">
                                    @foreach ($emergency->numbers as $number)
                                    <li><a href="tel:{{ $number->phone_number }}"><i class="fa fa-phone"></i> {{ $number->phone_number }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                        </div>
                        <div class="category-list">
                            <ul class="custom-list">
                                <li><a href="#" class="text-center embed-emergency" data-toggle="modal" data-target="#embedModal"><i class="fa fa-download"></i> Embed This on Your Website</a></li>
                            </ul>
                            <!-- Embed Modal -->
                            <div class="modal fade" id="embedModal" tabindex="-1" role="dialog"
                                 aria-labelledby="embedModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="embedModalLabel"><span class="fui-windows"></span> Embed Emergency Numbers</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Embed this emergency numbers into your website by using the folowing iframe:</p>
                                            <div class="form-group">
                                                <input onClick="this.setSelectionRange(0, this.value.length)" class="form-control"
                                                       value="<iframe src=&quot;{{ route('embed.emergency') }}&quot; width=&quot;425&quot; height=&quot;355&quot; frameborder=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot; style=&quot;border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;&quot; allowfullscreen>Loading numbers...</iframe>"
                                                       style="color:#34495e;" readonly />
                                            </div>
                                        </div> <!-- /,modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-embossed btn-wide btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <div style="margin-bottom: 20px"></div>
                </div>
            </div>
            <!-- End Page-Content -->
        </div>
    </div>
</div>
<!-- End Main-Content -->

@stop

@section('scripts')
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(function(){
            $('#category-content-emergency').slimScroll({
                height: '270px'
            });
        });
    });
</script>
@stop