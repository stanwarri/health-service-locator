<?php

class HealthCase extends Eloquent
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'health_cases';

    public function hospitals()
    {
        return $this->belongsToMany('Hospital', 'hospital_case', 'case_id', 'hospital_id');
    }

}
