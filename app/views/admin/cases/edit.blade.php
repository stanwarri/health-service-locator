@extends('layouts.admin')
@section('title', 'Editing Cases')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>Edit Health Case</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::model($case, array('role'=>'form', 'class' => 'form-horizontal row-fluid'))}}
        <div class="control-group">
            <label class="control-label" for="name">Case Name</label>
            <div class="controls">
                {{ Form::text('name', $case->name, array('class'=>'form-control'))}}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop