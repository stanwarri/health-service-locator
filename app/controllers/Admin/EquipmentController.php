<?php

namespace Admin;

use Illuminate\Support\Facades\View;
use Hsl\Repositories\EquipmentRepositoryInterface;

class EquipmentController extends \BaseController
{
    /**
     * Equipment repository.
     *
     * @var\Hsl\Repositories\EquipmentRepositoryInterface
     */
    protected $equipment;

    /**
     * Create a new EquipmentController instance.
     *
     * @param \Hsl\Repositories\EquipmentRepositoryInterface  $equipment
     * @return void
     */
    public function __construct(EquipmentRepositoryInterface $equipment)
    {
        parent::__construct();

        $this->equipment = $equipment;
    }

    /**
     * Show the equipment index page.
     *
     * @return \Response
     */
    public function getIndex()
    {
        $equipment = $this->equipment->findAll();

        return View::make('admin.equipment.list', compact('equipment'));
    }

    /**
     * Handle the creation of a equipment.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $form = $this->equipment->getCreateEquipmentForm();

        if (! $form->isValid()) {
            return $this->redirectRoute('admin.equipment.index')
                        ->withErrors($form->getErrors())
                        ->withInput();
        }

        $equipment = $this->equipment->create($form->getInputData());

        return $this->redirectRoute('admin.equipment.index');
    }
    
    /**
     * Show the equipment edit form.
     *
     * @param  mixed $id
     * @return \Response
     */
    public function edit($id)
    {
        $equipment = $this->equipment->findById($id);

        return View::make('admin.equipment.edit', compact('equipment'));
    }

    /**
     * Handle the editting of a equipment.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $form = $this->equipment->getCreateEquipmentForm();

        if (! $form->isValid()) {
            return $this->redirectRoute('admin.equipment.view', $id)
                        ->withErrors($form->getErrors())
                        ->withInput();
        }

        $equipment = $this->equipment->update($id, $form->getInputData());

        return $this->redirectRoute('admin.equipment.index');
    }
    
    
    /**
     * Delete a equipment from the database.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        $this->equipment->delete($id);

        return $this->redirectRoute('admin.equipment.index');
    }

}
