<?php

namespace Hsl\Repositories\Eloquent;

use Service;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Hsl\Services\Forms\ServiceCreateForm;
use Hsl\Repositories\ServiceRepositoryInterface;

class ServiceRepository extends AbstractRepository implements ServiceRepositoryInterface
{

    /**
     * Create a new DbTagRepository instance.
     *
     * @param  Service $services
     * @return void
     */
    public function __construct(Service $service)
    {
        $this->model = $service;
    }

    /**
     * Get an array of key-value (id => name) pairs of all services.
     *
     * @return array
     */
    public function listAll()
    {
        $services = $this->model->lists('name', 'id');

        return $services;
    }

    /**
     * Find all services.
     *
     * @param  string  $orderColumn
     * @param  string  $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|Service[]
     */
    public function findAll($orderColumn = 'created_at', $orderDir = 'desc')
    {
        $services = $this->model

                ->orderBy($orderColumn, $orderDir)
                ->get();
        return $services;
    }

    //Find service by ids
    public function getServicesByIds($ids)
    {
        if (!$ids) return null;
        return $this->model->whereIn('id', $ids)->get();
    }
    /**
     * Find a service by id.
     *
     * @param  mixed  $id
     * @return Service
     */
    public function findById($id)
    {
        if (!$id) return null;
        return $this->model->find($id);
    }

    /**
     * Find all services with the associated number of tips.
     *
     * @return \Illuminate\Database\Eloquent\Collection|Service[]
     */
    public function findAllWithTipCount()
    {
        return $this->model
                        ->leftJoin('tag_tip', 'services.id', '=', 'tag_tip.tag_id')
                        ->leftJoin('tips', 'tips.id', '=', 'tag_tip.tip_id')
                        ->groupBy('services.slug')
                        ->orderBy('tip_count', 'desc')
                        ->get([
                            'services.name',
                            'services.slug',
                            DB::raw('COUNT(tips.id) as tip_count')
                        ]);
    }

    /**
     * Create a new service in the database.
     *
     * @param  array  $data
     * @return Service
     */
    public function create(array $data)
    {
        $service = $this->getNew();

        $service->name = $data['name'];
        $service->slug = Str::slug($service->name, '-');

        $service->save();

        return $service;
    }

    /**
     * Update the specified service in the database.
     *
     * @param  mixed  $id
     * @param  array  $data
     */
    public function update($id, array $data)
    {
        $service = $this->findById($id);

        $service->name = $data['name'];
        $service->slug = Str::slug($service->name, '-');

        $service->save();

        return $service;
    }

    /**
     * Delete the specified service from the database.
     *
     * @param  mixed  $id
     * @return void
     */
    public function delete($id)
    {
        $service = $this->findById($id);
        $service->delete();
    }

    /**
     * Get the service create/update form service.
     *
     * @return \Codulab\Form\CreateTagForm
     */
    public function getCreateServiceForm()
    {
        return new ServiceCreateForm;
    }
}
