<?php

namespace SocialInterest\Presenters;

use Gravatar;
use McCool\LaravelAutoPresenter\BasePresenter;
use User;

class UserPresenter extends BasePresenter
{

    public function __construct(User $user)
    {
        $this->resource = $user;
    }

    public function getMarkdownBio()
    {
        return Markdown::render(e($this->resource->bio));
    }

    public function getFullName()
    {
        if ($this->resource->fullname) {
            return "(" . $this->resource->fullname . ')';
        }
        else {
            return '';
        }
    }

    public function profilePhoto()
    {
//        $userPhoto = $this->resource->photo;
//        if (is_null($userPhoto)) {
//            return Gravatar::src($this->resource->email, 400);
//        }
//        if ((substr($userPhoto, 0, 7) == "http://") || (substr($userPhoto, 0, 8) == "https://")) {
//            return $userPhoto;
//        }
//        else {
//            return url('files/photos/users/' . $userPhoto);
//        }
        return 'default_user.jpg';
    }

    public function coverPhoto()
    {
        return url('assets/uploads/users/default_cover.jpg');
    }

}

?>
