<?php

namespace Admin;

use State;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Jcf\Geocode\Facades\Geocode as Geocoder;
use Hsl\Repositories\HospitalRepositoryInterface;
use Hsl\Repositories\CaseRepositoryInterface;
use Hsl\Repositories\ServiceRepositoryInterface;
use Hsl\Repositories\EquipmentRepositoryInterface;

class HospitalsController extends \BaseController
{

    /**
     * Hospital repository.
     *
     * @var\Hsl\Repositories\HospitalRepositoryInterface
     */
    protected $hospitals;
    protected $cases;
    protected $services;
    protected $equipment;

    /**
     * Create a new HospitalsController instance.
     *
     * @param \Hsl\Repositories\HospitalRepositoryInterface  $hospitals
     * @return void
     */
    public function __construct(HospitalRepositoryInterface $hospitals, CaseRepositoryInterface $cases, ServiceRepositoryInterface $services, EquipmentRepositoryInterface $equipment)
    {
        parent::__construct();

        $this->hospitals = $hospitals;
        $this->cases = $cases;
        $this->services = $services;
        $this->equipment = $equipment;
    }

    /**
     * Show the hospitals index page.
     *
     * @return \Response
     */
    public function index()
    {
        $hospitals = $this->hospitals->findAll();

        return View::make('admin.hospitals.list', compact('hospitals'));
    }

    public function create()
    {
        $states = State::lists('name', 'id');
        $cases = $this->cases->listAll();
        $services = $this->services->listAll();
        $equipment = $this->equipment->listAll();
        return View::make('admin.hospitals.new', compact('states', 'cases', 'services', 'equipment'));
    }

    /**
     * Handle the creation of a hosipital.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $form = $this->hospitals->getCreateHospitalForm();

        if (!$form->isValid()) {
            return $this->redirectRoute('admin.hospitals.new')
                            ->withErrors($form->getErrors())
                            ->withInput();
        }
        $data = $form->getInputData();
        $geocode = Geocoder::make()->address(Input::get('address'));
        if ($geocode) {
            $data['latitude'] = $geocode->latitude();
            $data['longitude'] = $geocode->longitude();
        }
        else {
            $data['latitude'] = '6.533333';
            $data['longitude'] = '3.35';
        }

        if (Input::get('cases') == '') {
            $data['cases'] = array();
        }

        if (Input::get('services') == '') {
            $data['services'] = array();
        }

        if (Input::get('equipment') == '') {
            $data['equipment'] = array();
        }


        $hospital = $this->hospitals->create($data);

        return $this->redirectRoute('admin.hospitals.index');
    }

    /**
     * Show the hosipital edit form.
     *
     * @param  mixed $id
     * @return \Response
     */
    public function edit($id)
    {
        $hospital = $this->hospitals->findById($id);
        $states = State::lists('name', 'id');
        $cases = $this->cases->listAll();
        $selectedCases = $this->hospitals->listCasesIdsForHospital($hospital);
        $services = $this->services->listAll();
        $selectedServices = $this->hospitals->listServicesIdsForHospital($hospital);
        $equipment = $this->equipment->listAll();
        $selectedEquipment = $this->hospitals->listEquipmentIdsForHospital($hospital);
        return View::make('admin.hospitals.edit', compact('hospital', 'states', 'cases', 'selectedCases', 'services', 'selectedServices', 'equipment', 'selectedEquipment'));
    }

    /**
     * Handle the editting of a hosipital.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $form = $this->hospitals->getCreateHospitalForm();

        if (!$form->isValid()) {
            return $this->redirectRoute('admin.hospitals.edit', $id)
                            ->withErrors($form->getErrors())
                            ->withInput();
        }

        $data = $form->getInputData();
        $geocode = Geocoder::make()->address(Input::get('address'));
        if ($geocode) {
            $data['latitude'] = $geocode->latitude();
            $data['longitude'] = $geocode->longitude();
        }
        else {
            $data['latitude'] = '6.533333';
            $data['longitude'] = '3.35';
        }

        $hospital = $this->hospitals->update($id, $data);

        return $this->redirectRoute('admin.hospitals.index');
    }

    /**
     * Delete a hosipital from the database.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->hospitals->delete($id);

        return $this->redirectRoute('admin.hospitals.index');
    }

}
