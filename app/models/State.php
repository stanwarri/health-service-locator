<?php

class State extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'states';

    public function city()
    {
        return $this->hasMany('City');
    }

}
