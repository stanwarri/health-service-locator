@extends('layouts.admin')
@section('title', 'Manage Hospitals')

@section('content')
<a href="{{ route('admin.hospitals.new') }}" class="btn btn-primary btn-sm pull-right" style="margin-top:5px; margin-right: 10px">Add New Hospital</a> 

<div class="module">
    <div class="module-head">
        <h3>Hospitals</h3>
    </div>
    <div class="module-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Type</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($hospitals as $hospital)
                <tr>
                    <td>{{ $hospital->name }}</td>
                    <td>{{ $hospital->address }}</td>
                    <td>{{ $hospital->type }}</td>
                    <td>
                        <a href="{{ route('admin.hospitals.edit', [$hospital->id])}}" class="btn btn-mini btn-primary">
                            <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        <a rel="{{$hospital->id}}" class="delete_toggler btn btn-mini btn-danger">
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="pagination pagination-centered">
       {{ $hospitals->links() }} 
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete_hospital" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Hospitals</h4>
            </div>
            <div class="modal-body">
                <p class="lead text-center">Are you sure you want to delete this hospital</p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" href="#delete_hospital" class="btn btn-default">Keep</a>
                <a href="" id="delete_link" class="btn btn-danger">Delete</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
<script>    
    $(document).ready(function(){
        @if($errors->all())
            $('#add_hospital').modal('show');
        @endif

        // Populate the field with the right data for the modal when clicked
        $('.delete_toggler').each(function(index,elem) {
            $(elem).click(function(e){
                e.preventDefault();
                var href = "{{ route('admin.hospitals.delete') }}/";
                $('#delete_link').attr('href',href + $(elem).attr('rel'));
                $('#delete_hospital').modal('show');
            });
        });
    });
</script>
@stop
