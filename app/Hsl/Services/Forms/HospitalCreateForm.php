<?php

namespace Hsl\Services\Forms;

class HospitalCreateForm extends AbstractForm
{
    /**
     * The validation rules to validate the input data against.
     *
     * @var array
     */
    protected $rules = [
        'name' => 'required',
        'address' => 'required',
        'state' => 'required'
    ];
}
