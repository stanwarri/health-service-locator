@extends('layouts.main')
@section('title', 'Feedback')

@section('content')     
<section class="content content-light">
    <div class="container">
        <p class="text-center" style="padding-top: 30px">Help us improve this application. Kindly provide us with your feedback</p>

        <!-- Contact form -->
        <form action="#" class="contact-form" role="form">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control input-lg" placeholder="Enter your full name" />
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone No</label>
                        <input type="text" name="phone" class="form-control input-lg" placeholder="Enter your phone no" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control input-lg" placeholder="Enter your email address" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="message">Feedback</label>
                        <textarea name="message" class="form-control" rows="11" placeholder="Your feedback"></textarea>
                    </div>
                </div>
            </div>
            <p class="text-right buttons-margin-horizontal">

                <input type="submit" class="btn btn-theme btn-success" value="Send message" />
            </p>
        </form>
    </div>
</section>

@stop