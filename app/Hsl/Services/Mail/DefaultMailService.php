<?php

namespace Hsl\Services\Mail;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;
use Hsl\Repositories\UserRepositoryInterface;

/**
 * Description of DefaultMailService
 *
 * @author Stanley Warri
 */
class DefaultMailService implements MailServiceInterface
{

    protected $users;

    public function __construct(UserRepositoryInterface $users)
    {
        $this->users = $users;
        $this->mail_from = Config::get('config.site_email');
        $this->site_name = Config::get('config.site_name');
    }

    public function sendAccountRegisterEmail($to, $mail_from, $site_title, $link, $username)
    {
        Mail::send('emails.auth.activateAccount', [ 'link' => $link, 'username' => $username], function($message) use($username, $to, $mail_from, $site_title) {
                    $message->from($mail_from, $site_title);
                    $message->to($to, $username)->subject(Lang::get('reminders.activationEmailTitle'));
                }
        );
    }

    public function sendAdminAccountRegisterEmail($to, $mail_from, $site_name, $link, $temp_password, $fullname)
    {
        Mail::send('emails.auth.adminActivateAccount', ['link' => $link, 'fullname' => $fullname, 'password' => $temp_password], function($message) use($fullname, $to, $mail_from, $site_name) {
                    $message->from($mail_from, $site_name);
                    $message->to($to, $fullname)->subject('Activate your Email for Health Service Locator Access');
                }
        );
    }

    public function sendRecoverPasswordEmail($to, $mail_from, $site_title, $username, $password, $link)
    {
        Mail::send('emails.auth.recoverPassword', [
            'link' => $link,
            'username' => $username,
            'password' => $password], function($message) use ($username, $to, $mail_from, $site_title) {
                    $message->from($mail_from);
                    $message->to($to, $username)
                            ->subject(Lang::get('reminders.recoverPasswordEmailTitle'));
                });
    }

    public function sendAdminHealthStory($data)
    {
        $fullname = $data->fullname;
        $email = $data->email;
        $mail_from = $this->mail_from;
        $site_name = $this->site_name;
        $story = $data->story;

        Mail::send('emails.story', ['fullname' => $fullname, 'story' => $story], function($message) use($fullname, $email, $mail_from, $site_name) {
                    $message->from($mail_from, $site_name);
                    $message->to($email, $fullname)->subject('New Health Story');
                }
        );
    }

}
