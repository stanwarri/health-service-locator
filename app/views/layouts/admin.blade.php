<!DOCTYPE html>
<html lang="en">
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Health Service Locator - @yield('title')</title>
        <link type="text/css" href="{{ asset('assets/css/admin/bootstrap.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('assets/css/admin/bootstrap-responsive.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('assets/css/admin/style.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
        <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">
        @yield('styles')
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="{{ route('admin.dashboard') }}">HSL AdminCP</a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav pull-right">
                            <li><a>Last Login : {{ Auth::user()->last_login->format('l, jS F Y h:i:s A') }}</a></li>
                            <li><a href="{{ route('index') }}" target="_blank">Main Site </a></li>
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                {{ $currentUser->fullname }}
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('admin.users.account.edit') }}">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        @include('partials.admin-menu')
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            @yield('content')
                        </div>
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 The Nation Newspaper </b>All rights reserved.
            </div>
        </div>
        <script src="{{ asset('assets/js/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/admin/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/admin/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/admin/jquery.dataTables.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/admin/admin.js') }}" type="text/javascript"></script>
        @yield('scripts')
    </body>
</html>
