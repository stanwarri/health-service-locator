<?php

namespace Hsl\Services\Social;

use Hospital;
use RuntimeException;
use Illuminate\Config\Repository;
use Guzzle\Service\Client as GuzzleClient;
use Illuminate\Database\Eloquent\Collection;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Message\Request as GuzzleRequest;
use Guzzle\Http\QueryAggregator\DuplicateAggregator;

class Disqus
{
    /**
     * The curl client used for Disqus API interaction
     *
     * @var \Guzzle\Service\Client
     */
    protected $client;

    /**
     * Config repository.
     *
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * Create a new Disqus instance.
     *
     * @param  \Guzzle\Service\Client         $client
     * @param  \Illuminate\Config\Repository  $config
     * @return void
     */
    public function __construct(GuzzleClient $client, Repository $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * Get a config item.
     *
     * @param  mixed $key
     * @return mixed
     */
    protected function getConfig($key = null)
    {
        $key = is_null($key) ? '' : '.' . $key;

        return $this->config->get('social.disqus' . $key);
    }

    /**
     * Normalize the given hospital(s) to an array of hospitals.
     *
     * @param  mixed $hospitals
     * @return array
     */
    protected function getValidHospitals($hospitals)
    {
        if ($this->areInvalidHospitals($hospitals)) {
            throw new RuntimeException('Invalid hospitals');
        }

        if ($hospitals instanceof Hospital) {
            $hospitals = [ $hospitals ];
        }

        return $hospitals;
    }

    /**
     * Determine whether the given hospitals are invalid.
     *
     * @param  mixed  $hospitals
     * @return bool
     */
    protected function areInvalidHospitals($hospitals)
    {
        return ! $hospitals instanceof Hospital &&
               ! ($hospitals instanceof Collection && $hospitals->count() > 0);
    }

    /**
     * Get a formatted list of the hospital ids.
     *
     * @param  array $hospitals
     * @return array
     */
    protected function getThreadsArray($hospitals)
    {
        $threads = [];
        $format  = $this->getConfig('threadFormat');

        foreach ($hospitals as $hospital) {
            $threads[] = $format . $hospital->id;
        }

        return $threads;
    }

    /**
     * Prepare the query string before the API request.
     *
     * @param  \Guzzle\Http\Message\Request $request
     * @param  array $hospitals
     * @return \Guzzle\Http\Message\Request
     */
    protected function prepareQuery(GuzzleRequest $request, $hospitals)
    {
        $threads = $this->getThreadsArray($hospitals);
        $aggregator = $this->getQueryAggregator();

        $request->getQuery()
                ->set('forum', $this->getConfig('forum'))
                ->set('thread', $threads)
                ->set('api_key', $this->getConfig('publicKey'))
                ->setAggregator($aggregator);

        return $request;
    }

    /**
     * Get a new query aggregator instance.
     *
     * @return Guzzle\Http\QueryAggregator\DuplicateAggregator
     */
    protected function getQueryAggregator()
    {
        return new DuplicateAggregator;
    }

    /**
     * Get the response from the prepared request.
     *
     * @param  \Guzzle\Http\Message\Request $request
     * @return array
     */
    protected function getResponse($request)
    {
        try {
            $response = json_decode($request->send()->getBody(), true);

            return $response['response'];
        } catch (BadResponseException $bre) {
            return null;
        }
    }

    /**
     * Append the comment counts to the given hospitals.
     * @param  mixed $hospitals
     * @return mixed
     */
    public function appendCommentCounts($hospitals)
    {
        $hospitals = $this->getValidHospitals($hospitals);

        $request = $this->prepareQuery($this->client->get(), $hospitals);
        $response = $this->getResponse($request);

        if (is_null($response)) {
            foreach ($hospitals as $hospital) {
                $hospital->comment_count = 0;
            }
        } else {
            foreach ($response as $comment) {
                foreach ($hospitals as $hospital) {
                    if ($hospital->id == $comment['identifiers'][0]) {
                        $hospital->comment_count = $comment['posts'];
                        break;
                    }
                }
            }
        }

        return $hospitals instanceof Collection ? $hospitals : $hospitals[0];
    }
}
