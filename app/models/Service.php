<?php

class Service extends Eloquent
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';

    public function hospitals()
    {
        return $this->belongsToMany('Hospital', 'hospital_service', 'service_id', 'hospital_id');
    }

}
