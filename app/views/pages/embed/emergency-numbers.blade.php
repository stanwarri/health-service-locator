@extends('layouts.embed')
@section('title', 'Emergency Numbers')

@section('content')
<!-- Start Main-Content -->
<div class="main-content">
    <div class="categories list">
        <div class="container">
            <div class="row">
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-phone"></i></div>
                            <div class="category-title">
                                <h5>Emergency Numbers</h5>
                            </div>
                        </a>
                    </header>
                    <div id="category-content-emergency">
                        @foreach($emergencies as $emergency)
                        <h5>{{ $emergency->name }}</h5>
                        <div class="category-list">
                            <ul class="custom-list">
                                @foreach ($emergency->numbers as $number)
                                <li><a href="tel:{{ $number->phone_number }}"><i class="fa fa-phone"></i> {{ $number->phone_number }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endforeach
                    </div>
                    <div class="category-list">
                        <ul class="custom-list">
                            <li><a href="{{ route('index') }}" class="text-center embed-emergency"><i class="fa fa-download"></i> View on Website</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Categories-List -->
</div>
<!-- End Main-Content -->
@stop


@section('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(function(){
            $('#category-content-emergency').slimScroll({
                height: '270px'
            });
        });       
    });
</script>
@stop