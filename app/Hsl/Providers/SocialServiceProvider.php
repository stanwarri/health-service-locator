<?php

namespace Hsl\Providers;

use Hsl\Services\Social\Disqus;
use Illuminate\Support\ServiceProvider;
use Guzzle\Service\Client as GuzzleClient;

class SocialServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDisqus();
    }

    /**
     * Register the Disqus service.
     *
     * @return void
     */
    protected function registerDisqus()
    {
        $this->app['disqus'] = $this->app->share(function ($app) {
            $config = $app['config'];
            $client = new GuzzleClient($config->get('social.disqus.requestUrl'));

            return new Disqus($client, $config);
        });
    }
}
