@extends('layouts.main')
@section('title', $hospital->name)

@section('content')
<div class="map-wrapper">
    <!-- Start Map Canvas -->
    <div id="map_canvas_wrapper">
        <div id="map_canvas_hospital"></div>
    </div>
    <!-- End Map Canvas -->
</div>

<!-- Start Main-Content -->
<div id="company" class="main-content">
    <div class="container">
        <div class="row">
            <!-- Start Page-Content -->
            <div class="page-content">
                <div class="col-md-8">
                    <div class="row">            
                        <div class="col-md-2">
                            <div id="social">
                                <ul class="">
                                    <li><a href="http://www.facebook.com/sharer.php?u={{ urlencode(Request::url()) }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://plus.google.com/share?url={{ urlencode(Request::url()) }}" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::url()) }}&text={{{ $hospital->name }}}&via=http://thenationonlineng.net" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="company-preamble-content">
                                <h5 class="company-title"><a href="#">{{ $hospital->name }}</a></h5>
                                <p class="no-bottom">{{ $hospital->address }}</p>
                                <p class="no-bottom"><strong>Contact Information</strong></p>
                                <p>{{ $hospital->contact }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 custyle">
                        <table class="table table-striped custab">
                            <thead>
                            <a href="#" class="btn btn-primary btn-xs pull-right">Rate your experience in this hospital</a>
                            <tr>
                                <th>Health Case</th>
                                <th class="text-center">Rate</th>
                            </tr>
                            </thead>
                            @foreach($cases as $case)
                            <tr>
                                <td>{{ $case->name }}</td>
                                <td class="text-center">
                                    <?php $rates = App::make('RateController')->getHospitalRate('5', $hospital->id, $case->id, '25'); ?>
                                    {{ $rates }}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                var disqus_shortname = '{{ Config::get("config.disqus_shortname") }}';
                                var disqus_identifier = '{{$hospital->id}}';

                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category-box">
                        <header class="category-header garden clearfix">
                            <a href="#">
                                <div class="category-icon"><i class="fa fa-medkit"></i></div>
                                <div class="category-title">
                                    <h5>Emergency Cases</h5>
                                </div>
                            </a>
                        </header>
                        <div class="panel-group health-cases" id="accordion">
                            @foreach($cases as $case)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $case->id }}">
                                            {{ $case->name }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{ $case->id }}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="category-list">
                                            <ul class="custom-list">
                                                <?php
                                                $facilities = 0;
                                                foreach ($case->hospitals as $hospital) {
                                                    if ($facilities == 6)
                                                        break;
                                                    echo '<li><a href="' . route('hospital.view', $hospital->id) . '">' . $hospital->name . '</a></li>';

                                                    $facilities++;
                                                }
                                                ?>
                                                <li><a href="{{ route('health.case', $case->slug) }}" class="text-center"><i class="fa fa-arrow-circle-right"></i> View All</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <!--  Emergency Numbers-->

                    <div class="category-box">
                        <header class="category-header garden clearfix">
                            <a href="#">
                                <div class="category-icon"><i class="fa fa-phone"></i></div>
                                <div class="category-title">
                                    <h5>Emergency Numbers</h5>
                                </div>
                            </a>
                        </header>
                        <div id="category-content-emergency">
                            @foreach($emergencies as $emergency)
                            <h5>{{ $emergency->name }}</h5>
                            <div class="category-list">
                                <ul class="custom-list">
                                    @foreach ($emergency->numbers as $number)
                                    <li><a href="tel:{{ $number->phone_number }}"><i class="fa fa-phone"></i> {{ $number->phone_number }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                        </div>
                        <div class="category-list">
                            <ul class="custom-list">
                                <li><a href="#" class="text-center embed-emergency" data-toggle="modal" data-target="#embedModal"><i class="fa fa-download"></i> Embed This on Your Website</a></li>
                            </ul>
                            <!-- Embed Modal -->
                            <div class="modal fade" id="embedModal" tabindex="-1" role="dialog"
                                 aria-labelledby="embedModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="embedModalLabel"><span class="fui-windows"></span> Embed Emergency Numbers</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Embed this emergency numbers into your website by using the folowing iframe:</p>
                                            <div class="form-group">
                                                <input onClick="this.setSelectionRange(0, this.value.length)" class="form-control"
                                                       value="<iframe src=&quot;{{ route('embed.emergency') }}&quot; width=&quot;425&quot; height=&quot;355&quot; frameborder=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot; style=&quot;border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;&quot; allowfullscreen>Loading numbers...</iframe>"
                                                       style="color:#34495e;" readonly />
                                            </div>
                                        </div> <!-- /,modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-embossed btn-wide btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <div style="margin-bottom: 20px"></div>

                </div>
            </div>
            <!-- End Page-Content -->
        </div>
    </div>
</div>
<!-- End Main-Content -->

@stop


@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/rating.css') }}">
@stop

@section('scripts')
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/js/gomap.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(function(){
            $('#category-content-emergency').slimScroll({
                height: '270px'
            });
        });
        
        $("#map_canvas_hospital").each(function initialize() {
            $(this).goMap({
                maptype: 'ROADMAP',
                scrollwheel: false,
                navigationControl: false,
                zoom: 16,
                markers: [{
                        latitude: <?php echo $hospital->latitude; ?>,
                        longitude: <?php echo $hospital->longitude; ?>,
                        icon: '<?php echo URL::asset('assets/img/map-marker.png') ?>'
                    }]
            });
        });
        
        $("#main-wrapper.listing").each(function() {
            var map_width = $(window).width() - (60 + $("#main-wrapper").width());
            $("#map_canvas_wrapper").width(map_width);
        });

        $(window).resize(function(){
            var map_width = $(window).width() - (60 + $("#main-wrapper").width());
            $("#main-wrapper.listing #map_canvas_wrapper").width(map_width);
        });
    });
</script>
<script type="text/javascript">
    function rateMedia(hospitalId, caseId, rate, numStar, starWidth) {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $('#' + hospitalId + caseId + ' .star_bar #' + rate).removeAttr('onclick'); // Remove the onclick attribute: prevent multi-click
        $('.box' + hospitalId + caseId).html('<img src="{{ asset("assets/img/rating/loader-small.gif") }}" alt="" />'); // Display a processing icon
        var data = {hospital_id: hospitalId, case_id: caseId, rate: rate}; // Create JSON which will be send via Ajax
        $.ajax({ // JQuery Ajax
            type: 'POST',
            url:  base_url+'/rate/hospital', // URL to the PHP file which will insert new value in the database
            data: data, // We send the data string
            dataType: 'json',
            timeout: 3000,
            success: function(data) {
                $('.box' + hospitalId + caseId).html('<div style="font-size: small; color: green">Thank you for rating</div>'); // Return "Thank you for rating"
                // We update the rating score and number of rates
                $('.resultMedia' + hospitalId + caseId).html('<div style="font-size: small; color: grey">Rating: ' + data.avg + '/' + numStar + ' (' + data.nbrRate + ' votes)</div>');
                // We recalculate the star bar with new selected stars and unselected stars
                var nbrPixelsInDiv = numStar * starWidth;
                var numEnlightedPX = Math.round(nbrPixelsInDiv * data.avg / numStar);
                $('#' + hospitalId + caseId + ' .star_bar').attr('style', 'width:' + nbrPixelsInDiv + 'px; height:' + starWidth + 'px; background: linear-gradient(to right, #ffc600 0%,#ffc600 ' + numEnlightedPX + 'px,#ccc ' + numEnlightedPX + 'px,#ccc 100%);');
                $.each($('#' + hospitalId + caseId + ' .star_bar > div'), function () {
                    $(this).removeAttr('onmouseover onclick');
                });
            },
            error: function() {
                $('#box').text('Problem');
            }
        });
    }
     
    function overStar(hospitalId, caseId, myRate, numStar) {
        for ( var i = 1; i <= numStar; i++ ) {
            if (i <= myRate) $('#' + hospitalId + caseId + ' .star_bar #' + i).attr('class', 'star_hover');
            else $('#' + hospitalId + caseId + ' .star_bar #' + i).attr('class', 'star');
        }
    }
     
    function outStar(hospitalId, caseId, myRate, numStar) {
        for ( var i = 1; i <= numStar; i++ ) {
            $('#' + hospitalId + caseId + ' .star_bar #' + i).attr('class', 'star');
        }
    }
</script>
@stop