<?php

namespace Hsl\Repositories\Eloquent;

use HealthCase;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Hsl\Services\Forms\CaseCreateForm;
use Hsl\Repositories\CaseRepositoryInterface;

class CaseRepository extends AbstractRepository implements CaseRepositoryInterface
{

    /**
     * Create a new DbTagRepository instance.
     *
     * @param  Case $cases
     * @return void
     */
    public function __construct(HealthCase $case)
    {
        $this->model = $case;
    }

    /**
     * Get an array of key-value (id => name) pairs of all cases.
     *
     * @return array
     */
    public function listAll()
    {
        $cases = $this->model->lists('name', 'id');

        return $cases;
    }

    /**
     * Find all cases.
     *
     * @param  string  $orderColumn
     * @param  string  $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|Case[]
     */
    public function findAll($orderColumn = 'created_at', $orderDir = 'asc')
    {
        $cases = $this->model->orderBy($orderColumn, $orderDir)->get();
        return $cases;
    }

    //Find case by ids
    public function getCasesByIds($ids)
    {
        if (!$ids) return null;
        return $this->model->whereIn('id', $ids)->get();
    }
    /**
     * Find a case by id.
     *
     * @param  mixed  $id
     * @return Case
     */
    public function findById($id)
    {
        if (!$id) return null;
        return $this->model->find($id);
    }
    
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }
    
    public function findHospitalByCase(HealthCase $case)
    {
        return $case->hospitals()->where('case_id', $case->id)->orderBy('name', 'asc')->paginate(10);
    }
    
     public function findStateHospitalByCase(HealthCase $case)
    {
        return $case->hospitals()->where('case_id', $case->id)->orderBy('name', 'asc')->get();
    }

    /**
     * Create a new case in the database.
     *
     * @param  array  $data
     * @return Case
     */
    public function create(array $data)
    {
        $case = $this->getNew();

        $case->name = $data['name'];
        $case->slug = Str::slug($case->name, '-');

        $case->save();

        return $case;
    }

    /**
     * Update the specified case in the database.
     *
     * @param  mixed  $id
     * @param  array  $data
     */
    public function update($id, array $data)
    {
        $case = $this->findById($id);

        $case->name = $data['name'];
        $case->slug = Str::slug($case->name, '-');

        $case->save();

        return $case;
    }

    /**
     * Delete the specified case from the database.
     *
     * @param  mixed  $id
     * @return void
     */
    public function delete($id)
    {
        $case = $this->findById($id);
        $case->tips()->detach();
        $case->delete();
    }

    /**
     * Get the case create/update form case.
     *
     * @return \Codulab\Form\CreateTagForm
     */
    public function getCreateCaseForm()
    {
        return new CaseCreateForm;
    }
}
