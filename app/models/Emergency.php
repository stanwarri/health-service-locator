<?php

class Emergency extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emergencies';
    protected $fillable = [
        'name'
    ];

    public function numbers()
    {
        return $this->hasMany('Number');
    }

}
