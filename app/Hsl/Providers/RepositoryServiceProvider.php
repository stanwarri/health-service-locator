<?php

namespace Hsl\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
                'Hsl\Repositories\UserRepositoryInterface', 'Hsl\Repositories\Eloquent\UserRepository'
        );

        //Bind service repository
        $this->app->bind(
                'Hsl\Repositories\ServiceRepositoryInterface', 'Hsl\Repositories\Eloquent\ServiceRepository'
        );

        //Bind Health cases repository
        $this->app->bind(
                'Hsl\Repositories\CaseRepositoryInterface', 'Hsl\Repositories\Eloquent\CaseRepository'
        );

        //Bind Hospital repository
        $this->app->bind(
                'Hsl\Repositories\HospitalRepositoryInterface', 'Hsl\Repositories\Eloquent\HospitalRepository'
        );

        //Bind equipment repository
        $this->app->bind(
                'Hsl\Repositories\EquipmentRepositoryInterface', 'Hsl\Repositories\Eloquent\EquipmentRepository'
        );
    }

}

?>
