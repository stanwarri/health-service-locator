<?php

class Number extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emergency_number';
    protected $fillable = [
        'emergency_id',
        'phone_number'
    ];
    
    public function emergency()
    {
        return $this->belongsTo('Emergency');
    }

}
