@extends('layouts.admin') 
@section('title', 'Add New User')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>New Emergency Numbers</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::open(array('route' => 'admin.emergency.new', 'role' => 'form', 'class' => 'form-horizontal row-fluid')) }}
        <div class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
                {{ Form::text('name', null, array('class'=>'form-control'))}}
            </div>
        </div>
        <div class="control-group input_fields_wrap">
            <label class="control-label" for="numbers">Phone Numbers</label>
            <div class="controls">
                {{ Form::text('numbers', null, array('class'=>'form-control'))}}
                <span class="help-inline btn btn-default add_field_button">More Numbers</span>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="control-group input_fields_wrap"><div class="controls"><input type="text" class="form-control" name="numbers[]"/><span class="help-inline btn btn-danger remove_field">Remove</span></div></div></div>'); //add input box
            }
        });
   
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

</script>
@stop