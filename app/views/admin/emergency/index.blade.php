@extends('layouts.admin') 
@section('title', 'Emergency Numbers')

@section('content')
<a href="{{ route('admin.emergency.new') }}" class="btn btn-primary btn-sm pull-right" style="margin-top:5px; margin-right: 10px">Add Emergency Number</a> 

<div class="module">
    <div class="module-head">
        <h3>Manage Emergency Numbers</h3>
    </div>
    <div class="module-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                 @foreach($emergencies as $emergency)
                <tr>
                    <td>{{ $emergency->name }}</td>
                    <td>
                        <a href="" class="btn btn-mini btn-primary">
                            <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        <a rel="" class="delete_toggler btn btn-mini btn-danger">
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop

@section('scripts')

@stop