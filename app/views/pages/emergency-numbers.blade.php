@extends('layouts.main')
@section('title', 'Emergency Numbers')

@section('content')
<!-- Start Main-Content -->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="page-content col-md-8 col-md-offset-2">
                <div class="categories list">
                            <div class="category-box col-md-12">
                                <header class="category-header garden clearfix">
                                    <a href="#">
                                        <div class="category-icon"><i class="fa fa-phone"></i></div>
                                        <div class="category-title">
                                            <h5>Emergency Numbers</h5>
                                        </div>
                                    </a>
                                </header>
                                <div id="category-content-emergency">
                                    @foreach($emergencies as $emergency)
                                    <h5>{{ $emergency->name }}</h5>
                                    <div class="category-list">
                                        <ul class="custom-list">
                                            @foreach ($emergency->numbers as $number)
                                            <li><a href="tel:{{ $number->phone_number }}"><i class="fa fa-phone"></i> {{ $number->phone_number }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="category-list">
                                    <ul class="custom-list">
                                        <li><a href="{{ route('index') }}" class="text-center embed-emergency"><i class="fa fa-download"></i> View on Website</a></li>
                                    </ul>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main-Content -->
@stop

@section('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(function(){
            $('#category-content-emergency').slimScroll({
                height: '570px'
            });
        });       
    });
</script>
@stop