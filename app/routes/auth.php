<?php

Route::group(['before' => 'guest'], function() {
# Normal Login route
            Route::match(['GET'], 'auth/login', [
                'as' => 'auth.login',
                'uses' => 'AuthController@getLogin'
            ]);

            Route::match(['POST'], 'auth/login', [
                'uses' => 'AuthController@postLogin'
            ]);

//Normal Registration route
            Route::match(['GET'], 'auth/register', [
                'as' => 'auth.register',
                'uses' => 'AuthController@getRegister'
            ]);

            Route::match(['POST'], 'auth/register', [
                'uses' => 'AuthController@postRegister'
            ]);
            //Get remind password
            Route::match(['GET'], 'password/forgot', [
                'as' => 'auth.password.forgot',
                'uses' => 'AuthController@getPasswordForgot'
            ]);

            Route::post('password/forgot', array(
                'as' => 'auth.password.forgot',
                'uses' => 'AuthController@postPasswordForgot'
            ));

            Route::get('/password/reset/{code}', array(
                'as' => 'auth.password.reset',
                'uses' => 'AuthController@getRecoverPassword'
            ));

            Route::post('/password/reset', array(
                'as' => 'password.reset',
                'uses' => 'AuthController@postRecoverPassword'
            ));

            Route::get('/account/activate/{code}', array(
                'as' => 'account.activate',
                'uses' => 'AuthController@getActivate'
            ));
        });

//Logout route
Route::match(['GET'], 'auth/logout', [
    'as' => 'auth.logout',
    'uses' => 'AuthController@getLogout'
]);