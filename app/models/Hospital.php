<?php

class Hospital extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hospitals';
    protected $fillable = [
        'name',
        'address',
        'type',
        'latitude',
        'longitude',
        'contact',
        'state_id',
        'city_id'
    ];

    public function cases()
    {
        return $this->belongsToMany('HealthCase', 'hospital_case', 'hospital_id', 'case_id');
    }

    public function services()
    {
        return $this->belongsToMany('Service', 'hospital_service', 'hospital_id', 'service_id');
    }

    public function equipment()
    {
        return $this->belongsToMany('Equipment', 'hospital_equipment', 'hospital_id', 'equipment_id');
    }

}
