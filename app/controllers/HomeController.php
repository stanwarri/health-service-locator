<?php

use Hsl\Repositories\HospitalRepositoryInterface;
use Hsl\Repositories\CaseRepositoryInterface;
use Hsl\Repositories\ServiceRepositoryInterface;

Class HomeController extends BaseController
{

    protected $cases;
    protected $hospitals;
    protected $services;

    Public function __construct(HospitalRepositoryInterface $hospitals, CaseRepositoryInterface $cases, ServiceRepositoryInterface $services)
    {
        parent::__construct();
        $this->hospitals = $hospitals;
        $this->cases = $cases;
        $this->services = $services;
    }

    public function index()
    {
        $featured_story = Story::where('status', 1)->where('featured', 1)->orderBy('created_at', 'asc')->take(1)->get();
        
        $stories = Story::where('status', 1)->orderBy('featured', 'desc')->take(4)->get();
        
        Return View::make('pages.index', compact('featured_story', 'stories'));
    }

    public function getNearestHospitals()
    {
        $states = State::lists('name', 'id');
        $caseLists = $this->cases->listAll();
        $cases = $this->cases->findAll();
        $emergencies = Emergency::all();
        $news = $this->getHealthNewsFromRSS();

        //Start google maps configuration
        $config = array();
        $config['center'] = '6.4531, 3.3958';
        $config['zoom'] = 'auto';
        $config['cluster'] = TRUE;
        $config['scrollwheel'] = False;
        $config['onboundschanged'] = 'if(!centreGot){
        var mapCentre = map . getCenter();
        marker_0.setOptions( {
            position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        Gmaps::initialize($config);

        $hospitals = $this->hospitals->findByStateId(25);

        foreach ($hospitals as $hospital) {
            $marker = array();
            $marker['title'] = $hospital->name;
            $marker['position'] = $hospital->latitude . ',' . $hospital->longitude;
            $marker['clickable'] = TRUE;
            $marker['infowindow_content'] = '<h3>' . $hospital->name . '</h3><br>' . $hospital->address;
            if ($hospital->type == 'primary') {
                $marker['icon'] = asset('/assets/img/marker1.png');
            }
            elseif ($hospital->type == 'secondary') {
                $marker['icon'] = asset('/assets/img/marker2.png');
            }
            elseif ($hospital->type == 'tetiary') {
                $marker['icon'] = asset('/assets/img/marker2.png');
            }
            else {
                $marker['icon'] = asset('/assets/img/map-marker.png');
            }

            Gmaps::add_marker($marker);
        }

        $map = Gmaps::create_map();

        Return View::make('pages.health-locator', compact('states', 'caseLists', 'cases', 'emergencies', 'hospitals', 'news', 'map'));
    }

    public function getHospitalCase()
    {
        $caseId = Input::get('healthCase');
        //Start google maps configuration
        $config = array();
        $config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['cluster'] = TRUE;
        $config['onboundschanged'] = 'if(!centreGot){
        var mapCentre = map . getCenter();
        marker_0.setOptions( {
            position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        Gmaps::initialize($config);

        $case = $this->cases->findById($caseId);

        $hospitals = $this->cases->findStateHospitalByCase($case);

        foreach ($hospitals as $hospital) {
            $marker = array();
            $marker['title'] = $hospital->name;
            $marker['position'] = $hospital->latitude . ',' . $hospital->longitude;
            $marker['clickable'] = TRUE;
            $marker['infowindow_content'] = '<h3>' . $hospital->name . '</h3><br>' . $hospital->address;
            if ($hospital->type == 'primary') {
                $marker['icon'] = asset('/assets/img/marker1.png');
            }
            elseif ($hospital->type == 'secondary') {
                $marker['icon'] = asset('/assets/img/marker2.png');
            }
            elseif ($hospital->type == 'tetiary') {
                $marker['icon'] = asset('/assets/img/marker2.png');
            }
            else {
                $marker['icon'] = asset('/assets/img/map-marker.png');
            }

            Gmaps::add_marker($marker);
        }

        $map = Gmaps::create_map();

        return Response::json(array(
                    'success' => true,
                    'map' => $map), 200);
    }

    public function getHealthNewsFromRSS()
    {
        $feed = new SimplePie();
        $feed->set_feed_url("http://thenationonlineng.net/new/category/health/feed/");
        $feed->enable_cache(true);
        $feed->set_cache_location(storage_path() . '/cache');
        $feed->set_cache_duration(60 * 60 * 12);
        $feed->set_output_encoding('utf-8');
        $feed->init();

        return $feed;
    }

}

?>
