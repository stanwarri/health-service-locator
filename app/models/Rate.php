<?php

class Rate extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hospital_ratings';
    protected $fillable = [
        'hospital_id',
        'case_id',
        'rate',
        'IP'
    ];

}
