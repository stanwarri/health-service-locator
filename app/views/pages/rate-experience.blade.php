@extends('layouts.main')
@section('title', 'Rate your experience during any emergency')

@section('content')
<!-- Start Main-Content -->
<div class="main-content">
    <div class="container">
        @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12" style="margin-top: 150px; margin-bottom: 150px">
                <div class="alert alert-success text-center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
        @else
        <div class="row bs-wizard">
            <div class="page-content col-md-8 col-md-offset-2">
                {{ Form::open(array('id' => 'add-rate-form', 'class' => 'default-form', 'style' => 'margin-top:50px; margin-bottom:100px', 'files'=>true)) }}
                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Rate Your Experience During Any Emergency
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-lg-6 col-md-6">
                                <p class="form-row">
                                    {{ Form::text('fullname', null, array('id' => 'fullname', 'class' => 'required', 'placeholder' => 'Full Name*')) }}
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <p class="form-row">
                                    {{ Form::text('email', null, array('id' => 'email', 'class' => 'required email', 'placeholder' => 'Email Address*')) }}
                                </p>
                            </div>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                What type of emergency did you experience
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-lg-6">
                                <p class="form-row">
                                    <select name="health_case" id="health_case" class="required" data-placeholder="-Select Health Cases-">
                                        @foreach($caseLists as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </p>
                            </div><br/>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Where did you go for help?
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-md-12">
                                <p class="form-row">
                                    {{ Form::text('where_get_help', null, array('id' => 'where_get_help', 'class' => 'required', 'placeholder' => 'Where did you go for help?')) }}
                                </p>
                            </div>
                        </div> 
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                How would you rate the quality of care?
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-md-12">
                                <p class="form-row">
                                    <label class="radio-inline"><input type="radio" class="rate_get_help required" name="rate_get_help" value="Excellent">Excellent</label>
                                    <label class="radio-inline"><input type="radio" class="rate_get_help required" name="rate_get_help" value="Adequate">Adequate</label>
                                    <label class="radio-inline"><input type="radio" class="rate_get_help required" name="rate_get_help" value="Poor">Poor</label> 
                                </p>
                            </div>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Please provide an explanation on the quality of care
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-md-12">
                                <p class="form-row">
                                    {{ Form::textarea('explain_help', null, array('id' => 'explain_help', 'class' => 'required', 'placeholder' => 'Please provide an explanation on the quality of care')) }}
                                </p>
                            </div>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Was the facility well equipped? Please explain
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-md-12">
                                <p class="form-row">
                                    {{ Form::textarea('explain_facility', null, array('id' => 'explain_facility', 'class' => 'required', 'placeholder' => 'Was the facility well equipped? Please explain')) }}
                                </p>
                            </div>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Upload a photo of your experience
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-lg-12">
                                <div class="form-row">
                                    {{ Form::file('photo', ['id' => 'photo']) }}
                                </div>
                            </div>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>

                <div class="bs-step inv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Additional Comments (You can use this part to share with us your health story )
                            </h4>
                        </div>
                        <div class="panel-body bs-step-inner">
                            <div class="col-md-12">
                                <p class="form-row">
                                    {{ Form::textarea('share_story', null, array('id' => 'story', 'placeholder' => 'You can use this part to share with us your health story')) }}
                                </p>
                            </div>
                            <div class="box-footer">
                                <div class="clearfix"></div>
                                <button id="last-back" type="reset" class="btn btn-default">Go Back</button>
                                <button id="btn-signup" type="submit" class="btn btn-primary submit-btn">Submit</button> 
                            </div>
                        </div> <!--/.panel-body -->
                    </div> <!-- /.panel.panel-default -->
                </div>
                {{ Form::close() }}
            </div>
        </div>
        @endif
    </div>
</div>
<!-- End Main-Content -->
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/js/bs-wizard/bs-wizard-min.css') }}">
<style>
    .popover.error-popover {
        background-color: #F2DEDE;
        border-color: #EED3D7;
        box-shadow: none;
        color: #B94A48;
        cursor: pointer;
        max-width: none;
        z-index: 1099;
    }
    .popover.error-popover .popover-content {
        padding: 6px 14px;
    }
    .popover.error-popover.right .arrow {
        border-right-color: #EED3D7;
    }
    .popover.error-popover.right .arrow:after {
        border-right-color: #F2DEDE;
    }

    .popover.error-popover.top .arrow {
        border-top-color: #EED3D7;
        left: 30px;
    }
    .popover.error-popover.top .arrow:after {
        border-top-color: #F2DEDE;
    }
</style>
@stop

@section('scripts')
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/bs-wizard/bs-wizard.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.bootstrap.popover.min.js') }}"></script>
<script>
    $(function() {
        $(".bs-wizard").bs_wizard({beforeNext: before_next});
        $('#last-back').click($(".bs-wizard").bs_wizard('go_prev'));
 
        $('#add-rate-form').validate_popover({
            onsubmit: false
        });
        
        function validate_fields(fields, step) {
            var error_step, field, _i, _len;
            for (_i = 0, _len = fields.length; _i < _len; _i++) {
                field = fields[_i];
                if (!form_validator().element(field)) {
                    error_step = step;
                }
            }
            return error_step != null ? error_step : true;
        }

        function form_validator() {
            return $('#add-rate-form').validate();
        }

        function current_step() {
            return $(".bs-wizard").bs_wizard('option', 'currentStep');
        }

        function build_span_label(lable) {
            return "<span class='label label-success'>" + lable + "</span>";
        }
        
        function before_next() {
            if (current_step() == 1) return validate_fields($('#fullname, #email'), 1) === true;
            if (current_step() == 2) return validate_fields($('#health_case'), 1) === true;
            if (current_step() == 3) return validate_fields($('#where_get_help'), 1) === true;
            if (current_step() == 4) return validate_fields($('.rate_get_help'), 1) === true;
            if (current_step() == 5) return validate_fields($('#explain_help'), 1) === true;
            if (current_step() == 6) return validate_fields($('#explain_facility'), 1) === true;
            if (current_step() == 7) return validate_fields($('#photo'), 0) === true;
            return false;
        }
        
        $(window).resize(function() {
            $.validator.reposition();
        });
    });
</script>
<script type="text/javascript">	
    //    $(document).ready(function(){
    //        $('#add-rate-form').on('submit', function(e) {
    //            $.ajaxSetup({
    //                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    //            });
    //            e.preventDefault();
    //            fullname = $('#fullname').val();
    //            email = $('#email').val();
    //            health_case = $('#health_case').val();  
    //            where_get_help = $('#where_get_help').val();
    //            rate_get_help = $('#rate_get_help').val();
    //            explain_help = $('#explain_help').val();
    //            explain_facility = $('#explain_facility').val();
    //            photo = $('#photo').val();
    //            story = $('#story').val();
    //            $.ajax({
    //                type: "POST",
    //                enctype: 'multipart/form-data',
    //                url:  base_url+'/rate-experience',
    //                data: {fullname:fullname, email:email, health_case:health_case, where_get_help:where_get_help, rate_get_help:rate_get_help, explain_help:explain_help, explain_facility:explain_facility, photo:photo, story:story},
    //                //                data:  [{name : 'fullname', value : fullname}, {name : 'email', value : cur['result_type']}, 
    //                //                    {name : 'stateId_Code', value : cur['state']}, {name : 'lgaId_Code', value : cur['lga']}, {name : 'wardId_Code', value : cur['ward']}, {name : 'booth_code', value : cur['booth']}, {name : 'parties', value : partyResults}, {name : 'scores', value : partyScores}],
    //                success: function(response){
    //                    var $af = $('#result-submit-response');
    //                    $af.html('');
    //                    if(response.error){
    //                        $af.append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button><i class="fa fa-ban-circle fa-lg"></i><strong>' + response.message + ' <a href=' + response.route + '>Click here to edit this result</a></strong></div>');
    //                    }
    //                    if(response.success){
    //                        $af.append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button><i class="fa fa-ban-circle fa-lg"></i><strong>' + response.message + ' <a href=' + response.route + '>Click here to edit this result</a></strong></div>');
    //                    }
    //                }
    //            });
    //        });
    //    });
</script>
@stop