<?php


/**
 * Description of RecoverPasswordForm
 *
 * @author Stanley Warri
 */
namespace SocialInterest\Services\Forms;
use Illuminate\Config\Repository;

class RecoverPasswordForm extends AbstractForm 
{
    protected $config;
    
    protected $rules = array(
        'email'=>'required|max:50|email'
        );
    
    public function __construct(Repository $config)
    {
        parent::__construct();

        $this->config = $config;
    }
}
