<header role="banner" class="header header_4">
    <div class="h_top_part">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header_top mobile_menu">
                        <nav>
                            <ul>
                                <li><a href="http://thenationonlineng.net/new/" target="_blank">The Nation Online</a></li>
                                <li><a href="http://thenationonlineng.net/new/videos/" target="_blank">Videos</a></li>
                                <li><a href="http://thenationonlineng.net/new/subscribe-to-newsletter/" target="_blank">Subscribe to Newsletter</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="h_bot_part">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <a href="{{ route('index') }}" class="f_left logo"><img src="{{ asset('assets/img/logo.png') }}" alt=""></a>
                        <a href="http://www.wemabank.com/default.aspx" class="f_right"><img src="{{ asset('assets/img/WB-OA-Nation3_728.gif') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--main menu container-->
    <div class="menu_wrap">
        <div class="container clearfix">
            <!--button for responsive menu-->
            <button id="menu_button">
                Menu
            </button>
            <!--main menu-->
            <nav role="navigation" class="main_menu">  
                <ul>
                    <li class="current"><a href="{{ route('index') }}">Home</a></li>
                    <li><a href="{{ route('health.locator') }}">Nearest Hospitals</a></li>
                    <li><a href="{{ route('accident.hotspot') }}">Accident Hotspots</a></li>
                    <li><a href="{{ route('rate.experience') }}">Rate Your Experience</a></li>
                    <li><a href="{{ route('stories') }}">Health Stories</a></li>
                    <li><a href="{{ route('feedback') }}">Feedback</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>