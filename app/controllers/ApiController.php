<?php

use Hsl\Repositories\HospitalRepositoryInterface;

class ApiController extends BaseController
{

    protected $hospitals;

    /**
     * Create a new ApiController instance.
     */
    public function __construct(HospitalRepositoryInterface $hospitals)
    {
        parent::__construct();

        $this->hospitals = $hospitals;
    }

    public function getCities()
    {
        $state = Input::get('state');
        $cities = City::where('state_id', '=', $state)->lists('name', 'id');
        return Response::json($cities);
    }

    public function getHospitals($name)
    {
        $hospitals = $this->hospitals->search($name);
        return Response::json($hospitals);
    }

    public function getHospital($id)
    {
        $hospital = $this->hospitals->findById($id);
        return Response::json($hospital);
    }

    public function embedEmergencyNumbers()
    {
        $emergencies = Emergency::all();
        return View::make('pages.embed.emergency-numbers', compact('emergencies'));
    }

}
