@extends('layouts.main')
@section('title', 'Share your health story')

@section('content')
<!-- Start Main-Content -->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="page-content col-md-8 col-md-offset-2">
                <!-- Change the width and height values to suit you best -->
                <div class="typeform-widget" data-url="https://stanwarri.typeform.com/to/cbHbCW" data-text="Emergency" style="width:100%;height:500px;"></div>
                <script>(function(){var qs,js,q,s,d=document,gi=d.getElementById,ce=d.createElement,gt=d.getElementsByTagName,id='typef_orm',b='https://s3-eu-west-1.amazonaws.com/share.typeform.com/';if(!gi.call(d,id)){js=ce.call(d,'script');js.id=id;js.src=b+'widget.js';q=gt.call(d,'script')[0];q.parentNode.insertBefore(js,q)}})()</script>
                <div style="font-family: Sans-Serif;font-size: 12px;color: #999;opacity: 0.5; padding-top: 5px;">Powered by <a href="http://www.typeform.com/?utm_campaign=typeform_cbHbCW&amp;utm_source=website&amp;utm_medium=typeform&amp;utm_content=typeform-embedded&amp;utm_term=English" style="color: #999" target="_blank">Typeform</a></div>

                <!--                <div class="contact-form">
                                    <h5 class="title">Share your health story</h5>
                                    @include('partials.alerts')
                                    <div class="contact-form-box clearfix">
                                        {{ Form::open(array('class' => 'default-form')) }}
                                        <div class="col-lg-6 col-md-6">
                                            <p class="form-row">
                                                {{ Form::text('fullname', null, array('placeholder' => 'Full Name*')) }}
                                            </p>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <p class="form-row">
                                                {{ Form::text('email', null, array('placeholder' => 'Email Address*')) }}
                                            </p>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <p class="form-row">
                                                {{ Form::text('address', null, array('placeholder' => 'Address*')) }}
                                            </p>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <p class="form-row">
                                                {{ Form::text('phone', null, array('placeholder' => 'Phone*')) }}
                                            </p>
                                        </div>
                                        <div class="col-lg-12">
                                              <textarea class="ckeditor" name="story"></textarea>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-row">
                                                <label>Add an Image</label>
                                                {{ Form::file('featured_image') }}
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <button class="btn btn-info">
                                                <i class="fa fa-sign-out"></i>
                                                Submit
                                            </button>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>-->
                <!-- End Contact-Form -->
            </div>
        </div>
    </div>
</div>
<!-- End Main-Content -->
@stop

@section('styles')

@stop

@section('scripts')
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>
<script language="javascript">
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.htmlEncodeOutput = false
</script>
@stop