@extends('layouts.admin') 
@section('title', 'Add New Hospital')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>Add New Hospital</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::open(array('route' => 'admin.hospitals.new', 'class' => 'form-horizontal row-fluid')) }}
        <div class="control-group">
            <label class="control-label" for="states">States</label>
            <div class="controls">
                <select name="state" id="state" data-placeholder="Select State" class="span12 selectpicker">
                    @foreach($states as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>   
        <div class="control-group">
            <label class="control-label" for="lga">LGA</label>
            <div class="controls">
                <select id="city" name="city" data-placeholder="Select lga" class="span12 selectpicker">
                    <option value="">Select LGA</option>
                </select>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label" for="name">Hospital Name</label>
            <div class="controls">
                {{ Form::text('name', null, array('class'=>'span8','placeholder'=>'Hospital Name')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="address">Hospital Address</label>
            <div class="controls">
                {{ Form::text('address', null, array('class'=>'span8','placeholder'=>'Hospital Address')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="contact">Contact Info</label>
            <div class="controls">
                {{ Form::textarea('contact', null, array('class'=>'span8','placeholder'=>'Hospital Contact information')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="cases">Type</label>
            <div class="controls">
                <select name="type" class="span12 selectpicker" data-width="100%">
                    <option value="primary">Primary</option>
                    <option value="secondary" selected>Secondary</option>
                    <option value="tertiary">Tertiary</option>
                </select>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label" for="cases">Health Cases</label>
            <div class="controls">
                <select name="cases[]" class="span12 selectpicker" multiple title='Select Health Cases...' data-width="100%">
                    @foreach($cases as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label" for="services">Health Services</label>
            <div class="controls">
                <select name="services[]" class="span12 selectpicker" multiple title='Select Health Service...' data-width="100%">
                    @foreach($services as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label" for="equipment">Health Equipment</label>
            <div class="controls">
                <select name="equipment[]" class="span12 selectpicker" multiple title='Select Health Equipment...' data-width="100%">
                    @foreach($equipment as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" />
@stop

@section('scripts')
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript">
    ///////////////////////////////Enable bootstrap select
    $('.selectpicker').selectpicker();
    /////////////////////////////////////
    jQuery(document).ready(function($){
        $('#state').change(function(){
            $.get("{{ url('api/cities')}}", 
            { state: $(this).val() }, 
            function(data) {
                var city = $('#city');
                city.empty();
                city.append("<option value=''>Not Applicable</option>");
                $.each(data, function(key, value) {
                    city.append("<option value='"+ key +"'>" + value + "</option>");
                    $('#city').selectpicker('refresh');
                });

            });
        });
    });
</script>
@stop