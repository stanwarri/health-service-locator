<?php

namespace Hsl\Repositories\Eloquent;

use Hospital;
use Hsl\Services\Forms\HospitalCreateForm;
use Hsl\Repositories\HospitalRepositoryInterface;

class HospitalRepository extends AbstractRepository implements HospitalRepositoryInterface
{

    /**
     * Create a new DbTagRepository instance.
     *
     * @param  Hospital $hospitals
     * @return void
     */
    public function __construct(Hospital $hospital)
    {
        $this->model = $hospital;
    }

    /**
     * Get an array of key-value (id => name) pairs of all hospitals.
     *
     * @return array
     */
    public function listAll()
    {
        $hospitals = $this->model->lists('name', 'id');

        return $hospitals;
    }

    /**
     * Find all hospitals.
     *
     * @param  string  $orderColumn
     * @param  string  $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|Hospital[]
     */
    public function findAll($orderColumn = 'name', $orderDir = 'asc')
    {
        $hospitals = $this->model
                ->orderBy($orderColumn, $orderDir)
                ->paginate(10);
        return $hospitals;
    }

    //Find hospital by ids
    public function getHospitalsByIds($ids)
    {
        if (!$ids)
            return null;
        return $this->model->whereIn('id', $ids)->get();
    }

    /**
     * Find a hospital by id.
     *
     * @param  mixed  $id
     * @return Hospital
     */
    public function findById($id)
    {
        if (!$id)
            return null;
        return $this->model->find($id);
    }

    //Find By state Id
    public function findByStateId($id)
    {
        return $this->model->where('state_id', '=', $id)->get();
    }

    public function listCasesIdsForHospital(Hospital $hospital)
    {
        return $hospital->cases->lists('id');
    }

    public function listServicesIdsForHospital(Hospital $hospital)
    {
        return $hospital->services->lists('id');
    }

    public function listEquipmentIdsForHospital(Hospital $hospital)
    {
        return $hospital->equipment->lists('id');
    }

    /**
     * Create a new hospital in the database.
     *
     * @param  array  $data
     * @return Hospital
     */
    public function create(array $data)
    {
        $hospital = $this->getNew();

        $hospital->name = $data['name'];
        $hospital->address = $data['address'];
        $hospital->type = $data['type'];
        $hospital->latitude = $data['latitude'];
        $hospital->longitude = $data['longitude'];
        $hospital->contact = $data['contact'];
        $hospital->state_id = $data['state'];
        $hospital->city_id = $data['city'];

        $hospital->save();

        $hospital->cases()->sync($data['cases']);
        $hospital->services()->sync($data['services']);
        $hospital->equipment()->sync($data['equipment']);

        return $hospital;
    }

    /**
     * Update the specified hospital in the database.
     *
     * @param  mixed  $id
     * @param  array  $data
     */
    public function update($id, array $data)
    {
        $hospital = $this->findById($id);

        $hospital->name = $data['name'];
        $hospital->address = $data['address'];
        $hospital->type = $data['type'];
        $hospital->latitude = $data['latitude'];
        $hospital->longitude = $data['longitude'];
        $hospital->contact = $data['contact'];
        $hospital->state_id = $data['state'];
        $hospital->city_id = $data['city'];

        $hospital->save();

        $hospital->cases()->sync($data['cases']);
        $hospital->services()->sync($data['services']);
        $hospital->equipment()->sync($data['equipment']);

        return $hospital;
    }

    /**
     * Delete the specified hospital from the database.
     *
     * @param  mixed  $id
     * @return void
     */
    public function delete($id)
    {
        $hospital = $this->findById($id);
        $hospital->cases()->detach();
        $hospital->delete();
    }

    public function search($term)
    {
        $hospitals = $this->model
                ->orWhere('name', 'LIKE', '%' . $term . '%')
                ->orWhere('address', 'LIKE', '%' . $term . '%')
                ->orderBy('created_at', 'desc')
                ->orderBy('name', 'asc')
                ->lists('name', 'id');

        return $hospitals;
    }

    /**
     * Get the hospital create/update form hospital.
     *
     * @return \Codulab\Form\CreateTagForm
     */
    public function getCreateHospitalForm()
    {
        return new HospitalCreateForm;
    }
    
     /**
     * Find all tips that match the given search term.
     *
     * @param  string $term
     * @param  integer $perPage
     * @return \Illuminate\Pagination\Paginator|\Codulab\Model\Tip[]
     */
    public function searchByTermPaginated($term, $perPage = 12)
    {
        $hospitals = $this->model
                ->orWhere('name', 'LIKE', '%' . $term . '%')
                ->orWhere('address', 'LIKE', '%' . $term . '%')
                ->orWhereHas('cases', function ($query) use ($term) {
                            $query->where('name', 'LIKE', '%' . $term . '%')
                            ->orWhere('slug', 'LIKE', '%' . $term . '%');
                        })
                ->orWhereHas('services', function ($query) use ($term) {
                            $query->where('name', 'LIKE', '%' . $term . '%')
                            ->orWhere('slug', 'LIKE', '%' . $term . '%');
                        })
                ->orderBy('created_at', 'desc')
                ->orderBy('name', 'asc')
                ->paginate($perPage);

        return $hospitals;
    }

}
