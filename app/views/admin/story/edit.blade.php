@extends('layouts.admin') 
@section('title', 'Edit Story')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>Edit Story</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::open(array('class' => 'form-horizontal row-fluid', 'files' => true)) }}  
        <div class="control-group">
            <label class="control-label" for="title">Title</label>
            <div class="controls">
                {{ Form::text('title', $story->title, array('class'=>'span8','placeholder'=>'Title')) }}
            </div>
        </div>
        <div class="control-group">
            <textarea class="ckeditor" name="story">{{ $story->story }}</textarea>
        </div>
        <div class="control-group">
            <label class="control-label" for="featured_image">Featured Image</label>
            <div class="controls">
                {{ Form::file('featured_image') }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="video_link">Link to Video</label>
            <div class="controls">
                {{ Form::text('video_link', $story->video_link, array('class'=>'span8','placeholder'=>'Link to Youtube Video')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="accident_location">Location of Accident</label>
            <div class="controls">
                {{ Form::text('accident_location', null, array('class'=>'span8','placeholder'=>'Accident Location')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Story Options</label>
            <div class="controls">
                <label class="checkbox inline">
                    <input type="checkbox" name="status" value="1" checked>
                    Publish
                </label>
                <label class="checkbox inline">
                    <input type="checkbox" name="featured" value="1" @if($story->featured) = '1'){{ "checked" }} @endif>
                    Featured
                </label>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" />
@stop

@section('scripts')
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>
<script language="javascript">
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.htmlEncodeOutput = false
</script>

@stop