@extends('layouts.main')
@section('title', 'Health Stories')

@section('content')
<!-- Start Main-Content -->
<div id="blog" class="main-content">
    <div class="container">
        <div class="row">
            <div class="page-content col-lg-8 col-md-8 col-sm-8">
                <div class="blog-list">
                    @foreach($stories as $story)
                    <div class="blog-list-post">
                        <div class="blog-list-image">
                            <a href="{{ route('read.story', $story->slug) }}"><img src="{{ asset('assets/uploads/'.$story->featured_image.'') }}" alt=""></a>
                        </div>
                        <div class="blog-list-header clearfix">
                            <h5 class="title"><a href="{{ route('read.story', $story->slug) }}">{{ $story->title }}</a></h5>
                        </div>
                        <div class="blog-list-content clearfix">
                            <p>{{ str_limit(strip_tags($story->story), 354) }}</p>
                        </div>
                        <div class="blog-list-meta">
                            <span class="date">
                                <i class="fa fa-calendar"></i>{{ $story->created_at->format('l, jS F Y h:i:s A') }}
                            </span>
                            <span class="button">
                                <a href="{{ route('read.story', $story->slug) }}" class="btn btn-default"><i class="fa fa-arrow-circle-right"></i>Read More</a>
                            </span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- End Blog-List -->

            </div>
            <!-- Start Sidebar-Content -->
            <div class="sidebar-content col-lg-4 col-md-4 col-sm-4">
                <div class="category-box">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-medkit"></i></div>
                            <div class="category-title">
                                <h5>Emergency Cases</h5>
                            </div>
                        </a>
                    </header>
                    <div class="panel-group health-cases" id="accordion">
                        @foreach($cases as $case)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $case->id }}">
                                        {{ $case->name }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $case->id }}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="category-list">
                                        <ul class="custom-list">
                                            <?php
                                            $facilities = 0;
                                            foreach ($case->hospitals as $hospital) {
                                                if ($facilities == 6)
                                                    break;
                                                echo '<li><a href="' . route('hospital.view', $hospital->id) . '">' . $hospital->name . '</a></li>';

                                                $facilities++;
                                            }
                                            ?>
                                            <li><a href="{{ route('health.case', $case->slug) }}" class="text-center"><i class="fa fa-arrow-circle-right"></i> View All</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <!--  Emergency Numbers-->

                <div class="category-box">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-phone"></i></div>
                            <div class="category-title">
                                <h5>Emergency Numbers</h5>
                            </div>
                        </a>
                    </header>
                    <div id="category-content-emergency">
                        @foreach($emergencies as $emergency)
                        <h5>{{ $emergency->name }}</h5>
                        <div class="category-list">
                            <ul class="custom-list">
                                @foreach ($emergency->numbers as $number)
                                <li><a href="tel:{{ $number->phone_number }}"><i class="fa fa-phone"></i> {{ $number->phone_number }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endforeach
                    </div>
                    <div class="category-list">
                        <ul class="custom-list">
                            <li><a href="#" class="text-center embed-emergency" data-toggle="modal" data-target="#embedModal"><i class="fa fa-download"></i> Embed This on Your Website</a></li>
                        </ul>
                        <!-- Embed Modal -->
                        <div class="modal fade" id="embedModal" tabindex="-1" role="dialog"
                             aria-labelledby="embedModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        <h4 class="modal-title" id="embedModalLabel"><span class="fui-windows"></span> Embed Emergency Numbers</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Embed this emergency numbers into your website by using the folowing iframe:</p>
                                        <div class="form-group">
                                            <input onClick="this.setSelectionRange(0, this.value.length)" class="form-control"
                                                   value="<iframe src=&quot;{{ route('embed.emergency') }}&quot; width=&quot;425&quot; height=&quot;355&quot; frameborder=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot; style=&quot;border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;&quot; allowfullscreen>Loading numbers...</iframe>"
                                                   style="color:#34495e;" readonly />
                                        </div>
                                    </div> <!-- /,modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-embossed btn-wide btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>
                </div>
                <div style="margin-bottom: 20px"></div>
            </div>
            <!-- End Sidebar Content -->
        </div>
    </div>
</div>
<!-- End Main-Content -->

@stop

@section('scripts')
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(function(){
            $('#category-content-emergency').slimScroll({
                height: '450px'
            });
        });
    });
</script>
@stop