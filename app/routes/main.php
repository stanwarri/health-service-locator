<?php

Route::get('/', [
    'as' => 'index',
    'uses' => 'HomeController@index'
]);

Route::get('hospital/case', [
    'as' => 'hospital.case',
    'uses' => 'HomeController@getHospitalCase'
]);

Route::get('/about', [
    'as' => 'about',
    'uses' => 'PagesController@getAbout'
]);

Route::get('feedback', [
    'as' => 'feedback',
    'uses' => 'PagesController@feedback'
]);

Route::post('feedback', [
    'uses' => 'PagesController@postFeedback'
]);

Route::get('accident-hotspot', [
    'as' => 'accident.hotspot',
    'uses' => 'PagesController@getAccidentHotSpot'
]);

Route::get('nearest-hospitals', [
    'as' => 'health.locator',
    'uses' => 'HomeController@getNearestHospitals'
]);

Route::get('case/{slug}', [
    'as' => 'health.case',
    'uses' => 'PagesController@getHealthCase'
]);

Route::get('story/{slug}', [
    'as' => 'read.story',
    'uses' => 'PagesController@getStory'
]);

Route::get('/hospital/{id}', [
    'as' => 'hospital.view',
    'uses' => 'PagesController@getHospital'
]);


Route::get('rate-experience', [
    'as' => 'rate.experience',
    'uses' => 'PagesController@rateExperience'
]);

Route::post('rate-experience', [
    'uses' => 'PagesController@postRateExperience'
]);

Route::get('share-story', [
    'as' => 'share.story',
    'uses' => 'PagesController@shareStory'
]);

Route::post('share-story', [
    'uses' => 'PagesController@postStory'
]);

Route::get('stories', [
    'as' => 'stories',
    'uses' => 'PagesController@getStories'
]);


Route::get('emergency-numbers', [
    'as' => 'emergency.numbers',
    'uses' => 'PagesController@emergencyNumbers'
]);


Route::get('api/emergency-numbers', [
    'as' => 'embed.emergency',
    'uses' => 'ApiController@embedEmergencyNumbers'
]);

Route::get('hostpital/rate/{star}/{hospital_id}/{case_id}/{star_width}', [
    'as' => 'hospital.rate',
    'uses' => 'RateController@getHospitalRate'
]);

Route::post('rate/hospital', [
    'as' => 'post.hospital.rate',
    'uses' => 'RateController@postHospitalRate'
]);


# Search routes
Route::get('search', 'PagesController@getSearch');

Route::get('api/cities', 'ApiController@getCities');

Route::get('api/hospitals/{term}', 'ApiController@getHospitals');

Route::get('api/hospital/{id}', 'ApiController@getHospital');
