<?php

return array(
    /* 
    |--------------------------------------------------------------------------
    | These are config variables for Disqus commenting system
    |--------------------------------------------------------------------------
    | To use these you first have to create a new forum on Disqus: http://disqus.com/
    | Then copy and paste the PublicKey and the forum name found in the integrations.
    |
    */
    'disqus'   => array(
        'publicKey'    => 'xWO0jT377zgfdUsfUmdnyMxqbTxvmkexVzV8eeP8YijIsqXzUeqvLWhaBbhpNyuH',
        'forum'        => 'healthservicelocator',
        'requestUrl'   => 'http://disqus.com/api/3.0/threads/set.json',
        'threadFormat' => 'ident:',
    ),
);
