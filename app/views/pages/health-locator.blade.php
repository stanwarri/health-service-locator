@extends('layouts.main')
@section('title', 'Nearest Hospitals')
@section("body_onload", "")@stop

@section('content')
<!-- Start Welcome-Text -->
<div class="welcome-text">
    <div class="container">
        <div class="css-table">
            <div class="css-table-cell">
                <div class="welcome-logo">
                    <img src="{{ asset('assets/img/health-locator.png') }}" alt="">
                </div>
                <div class="welcome-content">
                    <h5><span>What is the emergency?</span></h5>
                    <p>Find the nearest hospital close to you right now from the map below. This service is only available for Lagos state</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Welcome-Text -->
<div class="map-wrapper">
    <div class="map-search">
        <div class="container">
            <!-- Start Form -->
            {{ Form::open(['url'=>'search','method'=>'GET', 'class' => 'default-form search-bar', 'role' => 'search']) }}
            <span class="location select-box"> <select name="State">
                    <option>Lagos State</option>
                </select> </span> <span class="category select-box">
                <select id="healthCase" name="caseList" data-placeholder="-Select Health Cases-">
                    <option>-Select Health Case-</option>
                    @foreach($caseLists as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </span>
            <span class="location_name" style="margin-left: 10px">
                <input type="text" name="q" placeholder="Search by Location, E.g Epe" value="{{{isset($term) ? $term : ''}}}">
            </span>
            <span class="submit-btn">
                <button class="btn btn-secondary full-width"><i class="fa fa-search"></i>Search Now</button>
            </span>
            {{ Form::close() }}
        </div>
    </div>
    <!-- End Map Search -->
    <!-- Start Map Canvas -->
    <div id="map_canvas_wrapper">
        <div id="map_canvas"></div>
    </div>
    <!-- End Map Canvas -->
</div>
<!-- End Map-Wrapper -->


<!-- Start Main-Content -->
<div class="main-content">
    <!-- Start Categories-List -->
    <div class="categories list">
        <div class="container">
            <div class="row">
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-medkit"></i></div>
                            <div class="category-title">
                                <h5>Emergency Cases</h5>
                            </div>
                        </a>
                    </header>
                    <div class="panel-group health-cases" id="accordion">
                        @foreach($cases as $case)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse{{ $case->id }}">
                                        {{ $case->name }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $case->id }}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="category-list">
                                        <ul class="custom-list">
                                            <?php
                                            $facilities = 0;
                                            foreach ($case->hospitals as $hospital) {
                                                if ($facilities == 6)
                                                    break;
                                                echo '<li><a href="' . route('hospital.view', $hospital->id) . '">' . $hospital->name . '</a></li>';

                                                $facilities++;
                                            }
                                            ?>
                                            <li>
                                                <a href="{{ route('health.case', $case->slug) }}"
                                                   class="text-center">
                                                    <i class="fa fa-arrow-circle-right"></i> View All
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-phone"></i></div>
                            <div class="category-title">
                                <h5>Emergency Numbers</h5>
                            </div>
                        </a>
                    </header>
                    <div id="category-content-emergency">
                        @foreach($emergencies as $emergency)
                        <h5>{{ $emergency->name }}</h5>
                        <div class="category-list">
                            <ul class="custom-list">
                                @foreach ($emergency->numbers as $number)
                                <li>
                                    <a href="tel:{{ $number->phone_number }}">
                                        <i class="fa fa-phone"></i> {{ $number->phone_number }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endforeach
                    </div>
                    <div class="category-list">
                        <ul class="custom-list">
                            <li>
                                <a href="#" class="text-center embed-emergency" data-toggle="modal"
                                   data-target="#embedModal">
                                    <i class="fa fa-download"></i> Embed This on Your Website
                                </a>
                            </li>
                        </ul>
                        <!-- Embed Modal -->
                        <div class="modal fade" id="embedModal" tabindex="-1" role="dialog"
                             aria-labelledby="embedModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">&times;</span><span
                                                class="sr-only">Close</span>
                                        </button>
                                        <h4 class="modal-title" id="embedModalLabel">
                                            <span class="fui-windows"></span> Embed Emergency Numbers</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Embed this emergency numbers into your website by using the folowing
                                            iframe:</p>

                                        <div class="form-group">
                                            <input onClick="this.setSelectionRange(0, this.value.length)"
                                                   class="form-control"
                                                   value="<iframe src=&quot;{{ route('embed.emergency') }}&quot; width=&quot;425&quot; height=&quot;355&quot; frameborder=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot; style=&quot;border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;&quot; allowfullscreen>Loading numbers...</iframe>"
                                                   style="color:#34495e;" readonly/>
                                        </div>
                                    </div>
                                    <!-- /,modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-embossed btn-wide btn-default"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                </div>
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-rss"></i></div>
                            <div class="category-title">
                                <h5>Health News</h5>
                            </div>
                        </a>
                    </header>
                    <div class="category-list">
                        <ul class="custom-list">
                            @foreach($news->get_items(0, 7) as $item)
                            <li>
                                <a href="{{ $item->get_permalink() }}"
                                   target="_blank"> {{ $item->get_title() }}</a>
                            </li>
                            @endforeach
                            <li>
                                <a href="http://thenationonlineng.net/new/category/health/" target="_blank"
                                   class="text-center">
                                    <i class="fa fa-arrow-circle-right"></i> More News
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main-Content -->
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/slider.css') }}">
@stop

@section('vendor_scripts')
<script src="{{ asset('assets/js/waypoints.js') }}"></script>
@stop

@section('scripts')
<?php echo $map['js']; ?>

<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(function () {
            $('#category-content-emergency').slimScroll({
                height: '270px'
            });
        });

        $('#healthCase').on('change', function () {
            initialize_map();
            $.get("{{ url('hospital/case')}}", {
                healthCase: $(this).val()
            }, function (data) {

            });
        });
    });

</script>
@stop