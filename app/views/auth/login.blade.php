@extends('layouts.auth')
@section('title', 'Login to Admincp')

@section('content') 
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="module module-login span4 offset4">
                @include('partials.alerts')
                {{ Form::open(array('route' => 'auth.login', 'class' => 'form-vertical')) }}
                <div class="module-head">
                    <h3>Admin Login</h3>
                </div>
                <div class="module-body">
                    <div class="control-group">
                        <div class="controls row-fluid">
                            <input class="span12" type="text" name="username" placeholder="Username or Email Address">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls row-fluid">
                            <input class="span12" type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                </div>
                <div class="module-foot">
                    <div class="control-group">
                        <div class="controls clearfix">
                            <button type="submit" class="btn btn-primary pull-right">Login</button>
                            <label class="checkbox">
                                <input type="checkbox" name="remember" value="1"> Remember me
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <span class="pull-right">
                            <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
                        </span>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div><!--/.wrapper-->

<!-- Modal -->
{{ Form::open(array('route' => 'auth.register', 'class' => 'form-login')) }}
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Forgot Password ?</h4>
            </div>
            <div class="modal-body">
                <p>Enter your e-mail address below to reset your password.</p>
                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                <button class="btn btn-success" type="button">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
{{ Form::close() }}
@stop
