@extends('layouts.admin') 
@section('title', 'Edit Hospital')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>Edit Hospital</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::open(array('class' => 'form-horizontal row-fluid')) }}
        <div class="control-group">
            <label class="control-label" for="states">States</label>
            <div class="controls">
                <select name="state" id="state" data-placeholder="Select State" class="span12 selectpicker">
                    @foreach($states as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label" for="lga">LGA</label>
            <div class="controls">
                <select id="city" name="city" data-placeholder="Select lga" class="span12 selectpicker">
                    <option value="">Select LGA</option>
                </select>
            </div>
        </div> 
        <div class="control-group">
            <label class="control-label" for="name">Hospital Name</label>
            <div class="controls">
                {{ Form::text('name', $hospital->name, array('class'=>'span8','placeholder'=>'Hospital Name')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="address">Hospital Address</label>
            <div class="controls">
                {{ Form::text('address', $hospital->address, array('class'=>'span8','placeholder'=>'Hospital Address')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="contact">Contact info</label>
            <div class="controls">
                {{ Form::textarea('contact', $hospital->contact, array('class'=>'span8','placeholder'=>'Hospital Phone No')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="cases">Type</label>
            <div class="controls">
                <select name="type" class="span12 selectpicker" data-width="100%">
                    <option value="primary">Primary</option>
                    <option value="secondary" selected>Secondary</option>
                    <option value="tertiary">Tertiary</option>
                </select>
            </div>
        </div> 
        <div class="control-group">
            {{ Form::label('cases', 'Health Cases', ['class' => 'control-label']) }}
            <div class="controls">
                {{ Form::select('cases[]', $cases, $selectedCases, array('multiple', 'class' => 'span12 selectpicker', 'data-width' => '100%', 'data-size' => '7', 'title' => 'Select Health Cases')) }}
            </div>
        </div>  
        <div class="control-group">
            {{ Form::label('services', 'Health Services', ['class' => 'control-label']) }}
            <div class="controls">
                {{ Form::select('services[]', $services, $selectedServices, array('multiple', 'class' => 'span12 selectpicker', 'data-width' => '100%', 'data-size' => '7', 'title' => 'Select Health Services')) }}
            </div>
        </div>  
        <div class="control-group">
            {{ Form::label('equipment', 'Health Equipment', ['class' => 'control-label']) }}
            <div class="controls">
                {{ Form::select('equipment[]', $equipment, $selectedEquipment, array('multiple', 'class' => 'span12 selectpicker', 'data-width' => '100%', 'data-size' => '7', 'title' => 'Select Health Equipment')) }}
            </div>
        </div>  
        <div class="control-group">
            <label class="control-label" for="latitude">Latitude</label>
            <div class="controls">
                {{ Form::text('latitude', $hospital->latitude, array('class'=>'span8', 'disabled')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="longitude">Longitude</label>
            <div class="controls">
                {{ Form::text('longitude', $hospital->longitude, array('class'=>'span8', 'disabled')) }}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" />
@stop

@section('scripts')
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript">
    ///////////////////////////////Enable bootstrap select
    $('.selectpicker').selectpicker();
    /////////////////////////////////////
    jQuery(document).ready(function($){
        $('#state').change(function(){
            $.get("{{ url('api/cities')}}", 
            { state: $(this).val() }, 
            function(data) {
                var city = $('#city');
                city.empty();
                city.append("<option value=''>Not Applicable</option>");
                $.each(data, function(key, value) {
                    city.append("<option value='"+ key +"'>" + value + "</option>");
                    $('#city').selectpicker('refresh');
                });

            });
        });
    });
</script>
@stop