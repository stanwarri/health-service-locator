<?php

namespace Admin;

use Illuminate\Support\Facades\View;

class DashboardController extends \BaseController
{
     
    public function dashboard()
    {
        return View::make('admin.dashboard');
    }

}
