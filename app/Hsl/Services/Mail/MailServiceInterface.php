<?php

namespace Hsl\Services\Mail;

/**
 *
 * @author Stanley Warri
 */
interface MailServiceInterface
{

    public function sendAccountRegisterEmail($to, $mail_from, $site_name, $link, $username);

    public function sendAdminAccountRegisterEmail($to, $mail_from, $site_name, $link, $temp_password, $fullname);

    public function sendRecoverPasswordEmail($to, $mail_from, $site_title, $username, $password, $link);
    
    public function sendAdminHealthStory($data);

}
