<?php

namespace Admin;

use Emergency;
use Number;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class EmergencyController extends \BaseController
{

    public function index()
    {
        $emergencies = Emergency::all();
        return View::make('admin.emergency.index', compact('emergencies'));
    }

    public function create()
    {
        return View::make('admin.emergency.create');
    }

    public function store()
    {
        $rules = array(
            'name' => 'required',
            'numbers' => 'required'
        );

        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return \Redirect::route('admin.emergency.new')
                            ->withErrors($validator)
                            ->withInput(Input::all());
        }

        $emergency = new Emergency;
        $emergency->name = Input::get('name');
        $emergency->save();

        $input_numbers = Input::only('numbers');
        foreach ($input_numbers as $keys => $id) {
            $number = new Number(array('phone_number' => $value));
            $emergency->numbers()->save($number);
        }

        return \Redirect::route('admin.emergency.index')->with('message', 'Emergency successfully addded');
    }

}
