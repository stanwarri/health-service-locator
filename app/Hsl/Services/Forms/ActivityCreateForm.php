<?php

namespace SocialInterest\Services\Forms;

class ActivityCreateForm extends AbstractForm
{

    protected $rules = array(
        'name' => 'required|min:10|max:150',
        'description' => 'required|min:10',
        'participant' => 'required|integer',
        'time' => 'required',
        'place' => 'required',
        'location' => 'required',
        'interests' => 'required|max_interests:3'
    );
    
    
    protected function beforeValidation()
    {
        \Validator::extend('max_interests', function($attribute, $interestIds, $params) {
                    $maxCount = $params[0];

                    $interestRepo = \App::make('SocialInterest\Repositories\InterestRepositoryInterface');
                    $interests = $interestRepo->findById($interestIds);

                    if ($interests->count() > $maxCount) {
                        return false;
                    }

                    return true;
                });
    }

}

?>
