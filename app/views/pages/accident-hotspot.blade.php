@extends('layouts.main')
@section('title', 'Accident Hotspots along Lagos-Ibadan Express Way')

@section('content')

<!-- Start Welcome-Text -->
<div class="welcome-text">
    <div class="container">
        <div class="css-table">
            <div class="css-table-cell">
                <div class="welcome-logo">
                    <img src="{{ asset('assets/img/battery.png') }}" alt="">
                </div>
                <div class="welcome-content">
                    <h5><span>Accident/Health Facilities Hotspots along Lagos-Ibadan Express Way</span></h5>
                    <p>This service tries to map the various danger spots that have experienced more accidents and also the nearest hospital/health facilities.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Welcome-Text -->
<div class="map-wrapper">
    <div id="map_canvas_wrapper">
        <div id="map_canvas"></div>
    </div>
</div>

<!-- Start Main-Content -->
<div class="main-content">

</div>
<!-- End Main-Content -->
@stop

@section('styles')

@stop

@section('scripts')
<?php echo $map['js']; ?>
@stop