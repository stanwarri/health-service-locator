<?php

namespace Hsl\Repositories;

use HealthCase;

interface CaseRepositoryInterface
{
    /**
     * Get an array of key-value (id => name) pairs of all tags.
     *
     * @return array
     */
    public function listAll();

    /**
     * Find all tags.
     *
     * @param  string $orderColumn
     * @param  string $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|\Case[]
     */
    public function findAll($orderColumn = 'created_at', $orderDir = 'desc');

    /**
     * Find a tag by id.
     *
     * @param  mixed $id
     * @return Case
     */
    public function findById($id);
    
    public function findBySlug($slug);
    
      //Find Hospitals by case
    public function findHospitalByCase(HealthCase $case);
    
    public function findStateHospitalByCase(HealthCase $case);

    /**
     * Create a new tag in the database.
     *
     * @param  array $data
     * @return Case
     */
    public function create(array $data);

    /**
     * Update the specified tag in the database.
     *
     * @param  mixed $id
     * @param  array $data
     * @return Case
     */
    public function update($id, array $data);

    /**
     * Delete the specified tag from the database.
     *
     * @param  mixed $id
     * @return void
     */
    public function delete($id);
}
