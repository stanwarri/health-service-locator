<?php

class Feedback extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feedbacks';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'message'
    ];

}
