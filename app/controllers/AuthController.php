<?php

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Hsl\Facades\MailService;
use Hsl\Repositories\UserRepositoryInterface;

class AuthController extends BaseController
{

    protected $users;

    /**
     * Create a new AuthController instance.
     */
    public function __construct(UserRepositoryInterface $users)
    {
        parent::__construct();

        $this->users = $users;
    }

    /**
     * Show login form.
     *
     * @return \Response
     */
    public function getLogin()
    {
        return View::make('auth.login');
    }

    /**
     * Post login form.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin()
    {
        $loginForm = $this->users->getLoginForm();
        if (!$loginForm->isValid()) {
            return Redirect::route('auth.login')
                            ->withErrors($loginForm->getErrors());
        }
        $credentials = Input::only(['username', 'password']);

        if (str_contains($credentials['username'], '@')) {
            $credentials['email'] = $credentials['username'];
            unset($credentials['username']);
        }

        $remember = Input::get('remember');

        if (!empty($remember)) {
            if (Auth::attempt($credentials, true)) {
                if (Auth::user()->activated != 1) {
                    Auth::logout();
                    return Redirect::route('auth.login')->with('message', 'Email activation is required');
                }
                $this->updateUserLogin(Auth::user()->id);
                return Redirect::route('admin.dashboard')->with('success', 'You are now logged in');
            }
        }
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if (Auth::user()->activated != 1) {
                Auth::logout();
                return Redirect::route('auth.login')->with('message', 'Email activation is required');
            }
            $this->updateUserLogin(Auth::user()->id);
            return Redirect::route('admin.dashboard')->with('success', 'You are now logged in');
        }
        return Redirect::route('auth.login')
                        ->withInput()
                        ->withErrors(Lang::get('Your username/password combination was incorrect'));
    }

    public function getActivate($code)
    {
        $user = $this->users->findByActivationCode($code);
        if ($user) {
            if ($this->users->setActivated($user)) {
                Auth::login($user);
                $this->updateUserLogin($user->id);
                return Redirect::route('admin.dashboard')->with('success', 'Account successfully activated');
            }
        }
        return Redirect::route('auth.login')->withErrors('Error activatng your account');
    }

    //Get password reset form
    public function getPasswordForgot()
    {
        return View::make('auth.forgot-password');
    }

    public function postPasswordForgot()
    {
        $recoverPasswordForm = $this->users->getRecoverPasswordForm();
        if (!$recoverPasswordForm->isValid()) {
            return Redirect::route('auth.password.forgot')
                            ->withInput()
                            ->withErrors($recoverPasswordForm->getErrors());
        }
        $data = $recoverPasswordForm->getInputData();
        $email = $data['email'];
        $user = $this->users->findByEmail($email);
        if ($user) {
            if ($this->users->setRecoverPasswordRequestState($user)) {
                $mail_from = Config::get('global.site_email');
                $site_title = Config::get('global.site_title');
                MailService::sendRecoverPasswordEmail($user->email, $mail_from, $site_title, URL::route('auth.password.reset', [$user->password_reset_code]), $user->username);
                return Redirect::route('auth.password.forgot')
                                ->with('message', [Lang::get('auth.account_recover_password_request_sent')]);
            }
        }
        else {
            return Redirect::route('auth.password.forgot')
                            ->withErrors(Lang::get('auth.account_email_notfound'))
                            ->withInput();
        }
    }

    public function getRecoverPassword($code)
    {
        if (is_null($code)) {
            App::abort(404);
        }
        $user = $this->users->findByPasswordResetCode($code);
        if ($user) {
            return View::make('auth.reset-password', ['code' => $code])->with('message', [Lang::get('success.account_recover_password')]);
        }
        return Redirect::route('auth.password.forgot')
                        ->withErrors([Lang::get('auth.password_reset_error_code')]);
    }

    public function postRecoverPassword()
    {
        $code = Input::get('code');
        if (is_null($code)) {
            return Redirect::route('auth.password.forgot');
        }
        $resetPasswordForm = $this->users->getResetPasswordForm();
        if (!$resetPasswordForm->isValid()) {
            return Redirect::route('auth.password.reset', [$code])
                            ->withInput()
                            ->withErrors($resetPasswordForm->getErrors());
        }

        $data = $resetPasswordForm->getInputData();

        $user = $this->users->findByPasswordResetCode($code);

        $reset = $this->users->setRecoverPasswordCompleteState($user, $data);
        if ($reset) {
            Auth::login($user);
            return Redirect::route('user.dashboard')->with('success', 'You are now logged in');
        }
    }

    public function updateUserLogin($id)
    {
        $oUser = $this->users->findbyId(Auth::user()->id);
        $oUser->last_login = \Carbon\Carbon::now();
        $oUser->save();
    }

    /**
     * Logout the user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        Auth::logout();
        return $this->redirectRoute('auth.login', [], [ 'logout_message' => true]);
    }

    //Get account activation
}
