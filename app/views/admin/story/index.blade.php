@extends('layouts.admin') 
@section('title', 'Health Stories')

@section('content')
<a href="{{ route('admin.story.new') }}" class="btn btn-primary btn-sm pull-right" style="margin-top:5px; margin-right: 10px">New Health Story</a> 

<div class="module">
    @include('partials.alerts')
    <div class="module-head">
        <h3>Health Stories</h3>
    </div>
    <div class="module-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Submitted By</th>
                    <th>Email</th>
                    <th>Date Submitted</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($stories as $story)
                <tr>
                    <td>{{ $story->title }}</td>
                    <td>{{ $story->fullname }}</td>
                    <td>{{ $story->email }}</td>
                    <td>{{ $story->created_at }}</td>
                    <td>
                        <a href="{{ route('admin.story.edit', [$story->id])}}" class="btn btn-mini btn-primary">
                            <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        <a rel="{{$story->id}}" class="delete_toggler btn btn-mini btn-danger">
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="delete_story" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Story</h4>
            </div>
            <div class="modal-body">
                <p class="lead text-center">Are you sure you want to delete this story</p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" href="#delete_story" class="btn btn-default">Keep</a>
                <a href="" id="delete_link" class="btn btn-danger">Delete</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
<script type="text/javascript">    
    $(document).ready(function(){
        // Populate the field with the right data for the modal when clicked
        $('.delete_toggler').each(function(index,elem) {
            $(elem).click(function(e){
                e.preventDefault();
                var href = "{{ route('admin.story.delete') }}/";
                $('#delete_link').attr('href',href + $(elem).attr('rel'));
                $('#delete_story').modal('show');
            });
        });
    });
</script>
@stop