<?php

use Illuminate\Database\Migrations\Migration;

class CreateCaseHospitalTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_case', function($table) {
                    $table->engine = 'InnoDB';

                    $table->increments('id')->unsigned();
                    $table->integer('case_id')->unsigned();
                    $table->integer('hospital_id')->unsigned();
                    $table->timestamps();

                    $table->foreign('case_id')
                            ->references('id')->on('health_cases')
                            ->onUpdate('cascade')
                            ->onDelete('cascade');

                    $table->foreign('hospital_id')
                            ->references('id')->on('hospitals')
                            ->onUpdate('cascade')
                            ->onDelete('cascade');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hospital_case', function($table) {
                    $table->dropForeign('hospital_case_case_id_foreign');
                    $table->dropForeign('hospital_case_hospital_id_foreign');
                });

        Schema::drop('hospital_case');
    }

}
