<?php

namespace Admin;

use Illuminate\Support\Facades\View;
use Hsl\Repositories\ServiceRepositoryInterface;

class ServicesController extends \BaseController
{
    /**
     * Service repository.
     *
     * @var\Hsl\Repositories\ServiceRepositoryInterface
     */
    protected $services;

    /**
     * Create a new ServicesController instance.
     *
     * @param \Hsl\Repositories\ServiceRepositoryInterface  $services
     * @return void
     */
    public function __construct(ServiceRepositoryInterface $services)
    {
        parent::__construct();

        $this->services = $services;
    }

    /**
     * Show the services index page.
     *
     * @return \Response
     */
    public function getIndex()
    {
        $services = $this->services->findAll();

        return View::make('admin.services.list', compact('services'));
    }

    /**
     * Handle the creation of a service.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $form = $this->services->getCreateServiceForm();

        if (! $form->isValid()) {
            return $this->redirectRoute('admin.services.index')
                        ->withErrors($form->getErrors())
                        ->withInput();
        }

        $service = $this->services->create($form->getInputData());

        return $this->redirectRoute('admin.services.index');
    }
    
    /**
     * Show the service edit form.
     *
     * @param  mixed $id
     * @return \Response
     */
    public function edit($id)
    {
        $service = $this->services->findById($id);

        return View::make('admin.services.edit', compact('service'));
    }

    /**
     * Handle the editting of a service.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $form = $this->services->getCreateServiceForm();

        if (! $form->isValid()) {
            return $this->redirectRoute('admin.services.view', $id)
                        ->withErrors($form->getErrors())
                        ->withInput();
        }

        $service = $this->services->update($id, $form->getInputData());

        return $this->redirectRoute('admin.services.index');
    }
    
    
    /**
     * Delete a service from the database.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        $this->services->delete($id);

        return $this->redirectRoute('admin.services.index');
    }

}
