  <!-- Start Footer -->
  <footer id="footer">
    <!-- Start Footer Copyrights -->
    <div class="footer-copyrights">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"><p>Copyright © 2014 The Nation Newspaper</p></div>
          <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
            <ul class="social pull-right custom-list">
              <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Footer Copyrights -->
  </footer>
  <!-- End Footer -->