<?php



namespace SocialInterest\Events;

use SocialInterest\Repositories\DateRepositoryInterface;
use Illuminate\Session\Store;

/**
 * Description of ViewDateHandler
 *
 * @author Stanley Warri
 */
class ViewDateHandler 
{
    protected $dates;
    protected $session;
    
    public function __construct(DateRepositoryInterface $dates,Store $session)
    {
        $this->dates = $dates;
        $this->session = $session;
    }
    
    
    public function handle($date)
    {
        if (! $this->hasViewedDate($date)) 
        {
            $date = $this->dates->incrementViews($date);

            $this->storeViewedTrick($date);
        }
    }


    protected function hasViewedDate($date)
    {
        return array_key_exists($date->id, $this->getViewedDates());
    }

    /**
* Get the users viewed trick from the session.
*
* @return array
*/
    protected function getViewedDates()
    {
        return $this->session->get('viewed_dates', []);
    }


    protected function storeViewedTrick($date)
    {
        $key = 'viewed_dates.' . $date->id;

        $this->session->put($key, time());
    }
}
