<?php

namespace Admin;

use Illuminate\Support\Facades\View;
use Hsl\Repositories\CaseRepositoryInterface;

class CasesController extends \BaseController
{
    /**
     * Case repository.
     *
     * @var\Hsl\Repositories\CaseRepositoryInterface
     */
    protected $cases;

    /**
     * Create a new CasesController instance.
     *
     * @param \Hsl\Repositories\CaseRepositoryInterface  $cases
     * @return void
     */
    public function __construct(CaseRepositoryInterface $cases)
    {
        parent::__construct();

        $this->cases = $cases;
    }

    /**
     * Show the cases index page.
     *
     * @return \Response
     */
    public function index()
    {
        $cases = $this->cases->findAll();

        return View::make('admin.cases.list', compact('cases'));
    }

    /**
     * Handle the creation of a case.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $form = $this->cases->getCreateCaseForm();

        if (! $form->isValid()) {
            return $this->redirectRoute('admin.cases.index')
                        ->withErrors($form->getErrors())
                        ->withInput();
        }

        $case = $this->cases->create($form->getInputData());

        return $this->redirectRoute('admin.cases.index');
    }
    
    /**
     * Show the case edit form.
     *
     * @param  mixed $id
     * @return \Response
     */
    public function edit($id)
    {
        $case = $this->cases->findById($id);

        return View::make('admin.cases.edit', compact('case'));
    }

    /**
     * Handle the editting of a case.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $form = $this->cases->getCreateCaseForm();

        if (! $form->isValid()) {
            return $this->redirectRoute('admin.cases.view', $id)
                        ->withErrors($form->getErrors())
                        ->withInput();
        }

        $case = $this->cases->update($id, $form->getInputData());

        return $this->redirectRoute('admin.cases.index');
    }
    
    
    /**
     * Delete a case from the database.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->cases->delete($id);

        return $this->redirectRoute('admin.cases.index');
    }

}
