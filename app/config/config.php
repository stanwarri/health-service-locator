<?php

return array(
    'site_name' => 'Health Service Locator',
    'site_email' => 'info@healthservicelocator.com.ng',
    'disqus_shortname' => 'healthservicelocator',
    // Some potential usernames should be protected and new or existing users will not
    // be able to take any of the following usernames :
    'forbidden_usernames' => [
        'hospitals',
        'hopsital',
        'user',
        'admin',
        'privacy',
        'license',
        'resources',
        'about',
        'login',
        'logout',
        'register',
        'img',
        'css',
        'js',
        'feed',
        'feed.atom',
        'feed.xml'
        ],
);
