<?php

namespace SocialInterest\Observers;

use SocialInterest\Repositories\UserRepositoryInterface;

class UserObserver extends AbstractObserver
{

    protected $users;

    public function __construct(
    UserRepositoryInterface $users
    )
    {
        $this->users = $users;
    }

    public function created($user)
    {
        \SocialInterest\Facades\MailService::sendNotificationToAdmin(['type' => 'New user registered',
            'username' => $user->username]);
    }

}
