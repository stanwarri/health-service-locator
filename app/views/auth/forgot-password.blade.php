@extends('layouts.auth')
@section('title', 'Forgot password')

@section('content') 
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="module module-login span4 offset4">
                @include('partials.alerts')
                {{ Form::open(array('route' => 'auth.password.forgot', 'class' => 'form-vertical')) }}
                <div class="module-head">
                    <h3>Admin Login</h3>
                </div>
                <div class="module-body">
                    <div class="control-group">
                        <div class="controls row-fluid">
                            {{ Form::text('email', null, ['class'=>'span12', 'placeholder' => 'Your Email', 'tabindex' => '1'])}}
                        </div>
                    </div>
                </div>
                <div class="module-foot">
                    <div class="control-group">
                        <div class="controls clearfix">
                            <button type="submit" class="btn btn-primary pull-right">Login</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div><!--/.wrapper-->
@stop
