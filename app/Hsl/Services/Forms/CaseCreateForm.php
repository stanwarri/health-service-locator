<?php

namespace Hsl\Services\Forms;

class CaseCreateForm extends AbstractForm
{
    /**
     * The validation rules to validate the input data against.
     *
     * @var array
     */
    protected $rules = [
        'name' => 'required'
    ];
}
