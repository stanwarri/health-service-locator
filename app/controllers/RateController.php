<?php

Class RateController extends BaseController
{

    public function getHospitalRate($numStar, $hospital_id, $case_id, $starWidth)
    {
        $cookie_name = 'hslRatingSystem' . $hospital_id . $case_id;
        $nbrPixelsInDiv = $numStar * $starWidth; // Calculate the DIV width in pixel
        // Rate average and number of rate from the database
//        $rate_query = DB::select(DB::raw("SELECT round(avg(rate), 2) AS average, count(rate) AS nbrRate FROM hospital_ratings WHERE hospital_id = :hospital_id & case_id = :case_id"), array(
//                    'hospital_id' => $hospital_id,
//                    'case_id' => $case_id
//                ));

        $rate = DB::table('hospital_ratings')
                ->select(DB::raw('round(avg(rate), 2) AS average, count(rate) AS nbrRate'))
                ->where('hospital_id', $hospital_id)
                ->where('case_id', $case_id)
                ->first();

        // dd($rate_query->average);
        //num of pixel to colorize (in yellow)
        $numEnlightedPX = round($nbrPixelsInDiv * $rate->average / $numStar, 0);
        $getJSON = array('numStar' => $numStar, 'hospitalId' => $hospital_id, 'caseId' => $case_id); // We create a JSON with the number of stars and the hospital & case ID
        $getJSON = json_encode($getJSON);
        $starBar = '<div id="' . $hospital_id . $case_id . '">';
        $starBar .= '<div class="star_bar" style="margin-left:130px; width:' . $nbrPixelsInDiv . 'px; height:' . $starWidth . 'px; background: linear-gradient(to right, #ffc600 0px,#ffc600 ' . $numEnlightedPX . 'px,#ccc ' . $numEnlightedPX . 'px,#ccc ' . $nbrPixelsInDiv . 'px);" rel=' . $getJSON . '>';
        for ($i = 1; $i <= $numStar; $i++) {
            $starBar .= '<div title="' . $i . '/' . $numStar . '" id="' . $i . '" class="star"';
            if (!isset($_COOKIE[$cookie_name]))
                $starBar .= ' onmouseover="overStar(' . $hospital_id . ', ' . $case_id . ', ' . $i . ', ' . $numStar . '); return false;" onmouseout="outStar(' . $hospital_id . ', ' . $case_id . ', ' . $i . ', ' . $numStar . '); return false;" onclick="rateMedia(' . $hospital_id . ', ' . $case_id . ', ' . $i . ', ' . $numStar . ', ' . $starWidth . '); return false;"';
            $starBar .= '></div>';
        }
        $starBar .= '</div>';
        $starBar .= '<div class="resultMedia' . $hospital_id . $case_id . '" style="font-size: small; color: grey">'; // We show the rate score and number of rates
        if ($rate->nbrRate == 0)
            $starBar .= 'Not rated yet';
        else
            $starBar .= 'Rating: ' . $rate->average . '/' . $numStar . ' (' . $rate->nbrRate . ' votes)';
        $starBar .= '</div>';
        $starBar .= '<div class="box' . $hospital_id . $case_id . '"></div>';
        $starBar .= '</div>';
        return $starBar;
    }

    public function postHospitalRate()
    {
        $hospitalId = Input::get('hospital_id');
        $caseId = Input::get('case_id');
        $rateNum = Input::get('rate');
        $expire = 24 * 3600; // 1 day
        setcookie('hslRatingSystem' . $hospitalId . $caseId, 'rated', time() + $expire, '/'); // Place a cookie
        Rate::create(array(
            'hospital_id' => $hospitalId,
            'case_id' => $caseId,
            'rate' => $rateNum,
            'IP' => $_SERVER['REMOTE_ADDR']
        ));
        $rate = DB::table('hospital_ratings')
                ->select(DB::raw('round(avg(rate), 2) AS average, count(rate) AS nbrRate'))
                ->where('hospital_id', $hospitalId)
                ->where('case_id', $caseId)
                ->first();

        $dataBack = array('avg' => $rate->average, 'nbrRate' => $rate->nbrRate);
        $dataBack = json_encode($dataBack);
        return $dataBack;
    }

}

?>
