<?php

class Equipment extends Eloquent
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'equipment';

    public function hospitals()
    {
        return $this->belongsToMany('Hospital', 'hospital_equipment', 'equipment_id', 'hospital_id');
    }

}
