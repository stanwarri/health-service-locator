<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>@yield('title') - Health Service Locator</title>
        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/header.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <link rel="icon" type="image/png" href="{{ asset('assets/img/fevicon.png') }}">
        <!-- Google Font -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,700,800,100,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <!--[if IE 9]>
          <script src="js/media.match.min.js"></script>
        <![endif]-->
        @yield('styles')
        <script type="text/javascript">
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-9422806-2', 'auto');
            ga('send', 'pageview');
        </script>   
        <script type="text/javascript"> var base_url = "{{ URL::to('/') }}" </script>
    </head>

    <body onload="@yield("body_onload")">
          <div id="main-wrapper">  

            @include('partials.header')

            @yield('content')

            @include('partials.main-footer')


        </div>
        <!-- End Main-Wrapper -->

        <!-- Start Back-To-Top Button -->
        <a href="#" id="back-to-top"><i class="fa fa-chevron-up"></i></a>
        <!-- End Back-To-Top Button -->

        <!-- Scripts -->
        <script src="{{ asset('assets/js/jquery-1.9.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.min.js') }}"></script>
        @yield('vendor_scripts')
        <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.matchHeight-min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.ba-outside-events.min.js') }}"></script>
        <script src="{{ asset('assets/js/scripts.js') }}"></script>
        @yield('scripts')
    </body>
</html>
