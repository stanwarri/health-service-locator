<?php

namespace Hsl\Services\Forms;

use Illuminate\Auth\AuthManager;
use Illuminate\Config\Repository;

class UserAccountSettingsForm extends AbstractForm
{

    /**
     * Config repository instance.
     *
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * Auth manager instance.
     *
     * @var \Illuminate\Auth\AuthManager
     */
    protected $auth;

    /**
     * The validation rules to validate the input data against.
     *
     * @var array
     */
    protected $rules = [
        'password' => 'confirmed|min:6',
        'password_confirmation' => 'same:password'
    ];

    /**
     * Create a new SettingsForm instance.
     *
     * @param  \Illuminate\Config\Repository  $config
     * @param  \Illuminate\Auth\AuthManager   $auth
     * @return void
     */
    public function __construct(Repository $config, AuthManager $auth)
    {
        parent::__construct();

        $this->config = $config;
        $this->auth = $auth;
    }

    /**
     * Get the prepared input data.
     *
     * @return array
     */
    public function getInputData()
    {
        return array_only($this->inputData, [
                    'email', 'password', 'password_confirmation'
                ]);
    }

}
