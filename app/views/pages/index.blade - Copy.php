@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
<div class="map-wrapper">
    <div class="map-search">
        <div class="container">
            <!-- Start Form -->
            <form action="" class="default-form search-bar">
                <span class="location select-box"> <select name="State">
                        <option>Lagos State</option>
                    </select> </span> <span class="category select-box">
                    <select name="caseList" data-placeholder="-Select Health Cases-">
                        <option>-Select Health Case-</option>
                        @foreach($caseLists as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select> </span> <span class="submit-btn">
                    <button class="btn btn-secondary full-width"><i class="fa fa-search"></i>Search Now</button>
                </span>
            </form>
        </div>
    </div>
    <!-- End Map Search -->

    <!-- Start Map Canvas -->
    <div id="map_canvas_wrapper">
        <div id="map_canvas"></div>
    </div>
    <!-- End Map Canvas -->
</div>
<!-- End Map-Wrapper -->


<!-- Start Main-Content -->
<div class="main-content">
    <!-- Start Categories-List -->
    <div class="categories list">
        <div class="container">
            <div class="row">
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-medkit"></i></div>
                            <div class="category-title">
                                <h5>Health Cases</h5>
                            </div>
                        </a>
                    </header>
                    <div class="panel-group health-cases" id="accordion">
                        @foreach($cases as $case)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $case->id }}">
                                        {{ $case->name }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $case->id }}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="category-list">
                                        <ul class="custom-list">
                                            <?php
                                            $facilities = 0;
                                            foreach ($case->hospitals as $hospital) {
                                                if ($facilities == 6)
                                                    break;
                                                echo '<li><a href="' . route('hospital.view', $hospital->id) . '">' . $hospital->name . '</a></li>';

                                                $facilities++;
                                            }
                                            ?>
                                            <li>
                                                <a href="{{ route('health.case', $case->slug) }}" class="text-center">
                                                    <i class="fa fa-arrow-circle-right"></i> View All
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-phone"></i></div>
                            <div class="category-title">
                                <h5>Emergency Numbers</h5>
                            </div>
                        </a>
                    </header>
                    <div id="category-content-emergency">
                        @foreach($emergencies as $emergency)
                        <h5>{{ $emergency->name }}</h5>
                        <div class="category-list">
                            <ul class="custom-list">
                                @foreach ($emergency->numbers as $number)
                                <li>
                                    <a href="tel:{{ $number->phone_number }}">
                                        <i class="fa fa-phone"></i> {{ $number->phone_number }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endforeach
                    </div>
                    <div class="category-list">
                        <ul class="custom-list">
                            <li>
                                <a href="#" class="text-center embed-emergency" data-toggle="modal" data-target="#embedModal">
                                    <i class="fa fa-download"></i> Embed This on Your Website
                                </a>
                            </li>
                        </ul>
                        <!-- Embed Modal -->
                        <div class="modal fade" id="embedModal" tabindex="-1" role="dialog"
                             aria-labelledby="embedModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        <h4 class="modal-title" id="embedModalLabel">
                                            <span class="fui-windows"></span> Embed Emergency Numbers</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Embed this emergency numbers into your website by using the folowing iframe:</p>

                                        <div class="form-group">
                                            <input onClick="this.setSelectionRange(0, this.value.length)" class="form-control"
                                                   value="<iframe src=&quot;{{ route('embed.emergency') }}&quot; width=&quot;425&quot; height=&quot;355&quot; frameborder=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot; style=&quot;border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;&quot; allowfullscreen>Loading numbers...</iframe>"
                                                   style="color:#34495e;" readonly/>
                                        </div>
                                    </div>
                                    <!-- /,modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-embossed btn-wide btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                </div>
                <div class="category-box col-lg-4 col-md-4 col-sm-6">
                    <header class="category-header garden clearfix">
                        <a href="#">
                            <div class="category-icon"><i class="fa fa-rss"></i></div>
                            <div class="category-title">
                                <h5>Health News</h5>
                            </div>
                        </a>
                    </header>
                    <div class="category-list">
                        <ul class="custom-list">
                            @foreach($news->get_items(0, 7) as $item)
                            <li>
                                <a href="{{ $item->get_permalink() }}" target="_blank"> {{ $item->get_title() }}</a>
                            </li>
                            @endforeach
                            <li>
                                <a href="http://thenationonlineng.net/new/category/health/" target="_blank" class="text-center">
                                    <i class="fa fa-arrow-circle-right"></i> More News
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main-Content -->
@stop


@section('scripts')
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/js/gomap.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(function() {
            $('#category-content-emergency').slimScroll({
                height: '270px'
            });
        });

        $('#state').change(function() {
            $.get("{{ url('api/cities')}}",
            {state: $(this).val()},
            function(data) {
                var city = $('#city');
                city.empty();
                $.each(data, function(key, value) {
                    city.append("<option value='" + key + "'>" + value + "</option>");
                });

            });
        });

        // MAIN MAP
        $("#map_canvas").each(function() {
            $(this).goMap({
                maptype: 'ROADMAP',
                scrollwheel: false,
                navigationControl: false,
                zoom: 10,
                markers: [
<?php foreach ($hospitals as $hospital): ?>
                        {
                            latitude: <?php echo $hospital['latitude'] ?>,
                            longitude:<?php echo $hospital['longitude'] ?>,
                            @if($hospital['type'] == 'secondary')
                                icon: '<?php echo URL::asset('assets/img/secondary_marker.png') ?>',
                            @endif
                            @if($hospital['type'] == 'primary')
                                icon: '<?php echo URL::asset('assets/img/primary_marker.png') ?>',
                            @endif
                            html: '<div class="marker-holder">' +
                                '<div class="map-item-info">' +
                                '<h5 class="title"><a href="#"><?php echo $hospital['name'] ?></a></h5>' +
                                '<div class="describe">' +
                                '<p class="contact-info address"><?php echo $hospital['address'] ?></p>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                        },
<?php endforeach; ?>
                ]
            });
        });

        // Get current user location
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                // Place a marker on the current user location
                $.goMap.createMarker({
                    html: 'Your Current Location',
                    icon: '{{ url('assets/img/marker3.png') }}',
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            });
        }

        $("#main-wrapper.listing").each(function() {
            var map_width = $(window).width() - (60 + $("#main-wrapper").width());
            $("#map_canvas_wrapper").width(map_width);
        });

        $(window).resize(function() {
            var map_width = $(window).width() - (60 + $("#main-wrapper").width());
            $("#main-wrapper.listing #map_canvas_wrapper").width(map_width);
        });
    });
</script>
@stop







include('db.class.php');
$bdd = new db();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<title>Very precise jQuery/Ajax Star Rating Plugin Tutorial</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
<script type="text/javascript" src="jquery/jquery-1.10.1.min.js"></script>
<style>

</style>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<br /><br />
<?php
function starBar($numStar, $mediaId, $starWidth) {
global $bdd;
$cookie_name = 'tcRatingSystem2'.$mediaId;
$nbrPixelsInDiv = $numStar * $starWidth; // Calculate the DIV width in pixel
// Rate average and number of rate from the database
$query = $bdd->getOne('SELECT round(avg(rate), 2) AS average, count(rate) AS nbrRate FROM tc_tuto_rating WHERE media='.$mediaId.'');
//num of pixel to colorize (in yellow)
$numEnlightedPX = round($nbrPixelsInDiv * $query['average'] / $numStar, 0);
$getJSON = array('numStar' => $numStar, 'mediaId' => $mediaId); // We create a JSON with the number of stars and the media ID
$getJSON = json_encode($getJSON);
 
$starBar = '<div id="'.$mediaId.'">';
$starBar .= '<div class="star_bar" style="width:'.$nbrPixelsInDiv.'px; height:'.$starWidth.'px; background: linear-gradient(to right, #ffc600 0px,#ffc600 '.$numEnlightedPX.'px,#ccc '.$numEnlightedPX.'px,#ccc '.$nbrPixelsInDiv.'px);" rel='.$getJSON.'>';
for ($i=1; $i<=$numStar; $i++) {
$starBar .= '<div title="'.$i.'/'.$numStar.'" id="'.$i.'" class="star"';
if( !isset($_COOKIE[$cookie_name]) ) $starBar .= ' onmouseover="overStar('.$mediaId.', '.$i.', '.$numStar.'); return false;" onmouseout="outStar('.$mediaId.', '.$i.', '.$numStar.'); return false;" onclick="rateMedia('.$mediaId.', '.$i.', '.$numStar.', '.$starWidth.'); return false;"';
$starBar .= '></div>';
}
$starBar .= '</div>';
$starBar .= '<div class="resultMedia'.$mediaId.'" style="font-size: small; color: grey">'; // We show the rate score and number of rates
if ($query['nbrRate'] == 0) $starBar .= 'Not rated yet';
else $starBar .= 'Rating: ' . $query['average'] . '/' . $numStar . ' (' . $query['nbrRate'] . ' votes)';
$starBar .= '</div>';
$starBar .= '<div class="box'.$mediaId.'"></div>';
$starBar .= '</div>';
return $starBar;
}
// Display 4 star bar system for 4 different IDs
echo starBar(3, 502, 25); // 3 stars, Media ID 200, 25px star image
echo starBar(5, 201, 25); // 5 stars, Media ID 201, 25px star image
echo starBar(10, 202, 25); // 10 stars, Media ID 202, 25px star image
echo starBar(30, 203, 25); // 30 stars, Media ID 203, 25px star image
?>
</div>
</div>
</div>
<script type="text/javascript" src="js/precise-star-rating.js"></script>
</body>
</html>