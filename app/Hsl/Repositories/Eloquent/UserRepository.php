<?php

namespace Hsl\Repositories\Eloquent;

use User;
use Illuminate\Support\Facades\Hash;
use Hsl\Repositories\UserRepositoryInterface;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    //Get all users from the database
    public function findAll()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function findById($id)
    {
        return $this->model->find($id);
    }

    public function findByUsername($username)
    {
        return $this->model->whereUsername($username)->first();
    }

    public function findByEmail($email)
    {
        return $this->model->whereEmail($email)->first();
    }

    public function adminCreateuser(array $data)
    {
        $user = User::create(array(
                    'email' => $data['email'],
                    'fullname' => $data['fullname'],
                    'username' => $data['username'],
                    'password' => Hash::make($data['password']),
                    'activation_code' => str_random(60),
                    'activated' => 1,
                    'is_admin' => 1
                ));
        return $user;
    }

    public function findByActivationCode($code)
    {
        return $this->model
                        ->where('activation_code', '=', $code)
                        ->where('activated', '=', 0)
                        ->first();
    }

    public function setActivated($user)
    {
        $user->activated = 1;
        $user->activation_code = '';
        return $user->save();
    }

    public function setRecoverPasswordRequestState($user)
    {
        $generatedPassword = str_random(60);
        $user->password_reset_code = $generatedPassword;
        return $user->save();
    }

    public function findByPasswordResetCode($code)
    {
        return $this->model->where('password_reset_code', '=', $code)->first();
    }

    public function setRecoverPasswordCompleteState($user, $data)
    {
        $user->password = Hash::make($data['password']);
        $user->password_reset_code = '';

        return $user->save();
    }

    /**
     * Update the user's settings.
     *
     */
    public function updateAccountSettings(User $user, array $data)
    {
        $user->password = ($data['password'] != '') ? Hash::make($data['password']) : $user->password;

        return $user->save();
    }

    public function updateSettings($user, array $data)
    {
        $user->password = ($data['password'] != '') ? Hash::make($data['password']) : $user->password;
        $user->save();
        return $user;
    }

    public function delete($id)
    {
        $user = $this->findById($id);
        $user->delete();
    }

    public function getLoginForm()
    {
        return app('Hsl\Services\Forms\LoginForm');
    }

    public function getAdminAccountCreateForm()
    {
        return app('Hsl\Services\Forms\AdminAccountCreateForm');
    }

    public function getAccountCreateForm()
    {
        return app('Hsl\Services\Forms\AccountCreateForm');
    }

    public function getAccountEditSettingsForm()
    {
        return app('Hsl\Services\Forms\AccountEditSettingsForm');
    }

    public function getRecoverPasswordForm()
    {
        return app('Hsl\Services\Forms\RecoverPasswordForm');
    }

    public function getResetPasswordForm()
    {
        return app('Hsl\Services\Forms\ResetPasswordForm');
    }

    public function getAccountSettingsForm()
    {
        return app('Hsl\Services\Forms\UserAccountSettingsForm');
    }

}

?>
