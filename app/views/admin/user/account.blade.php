@extends('layouts.admin') 
@section('title', 'Edit Account')

@section('content')

<div class="module">
    <div class="module-head">
        <h3>Add New User</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::open(array('class' => 'form-horizontal row-fluid')) }}
        <div class="control-group">
            <label class="control-label" for="fullname">Full Name</label>
            <div class="controls">
                {{ Form::text('fullname', $user->fullname, array('class'=>'span8','placeholder'=>'Full Name', 'disabled' => 'true')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="email">Email Address</label>
            <div class="controls">
                {{ Form::text('email', $user->email, array('class'=>'span8','placeholder'=>'Email Address', 'disabled' => 'true')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                {{ Form::password('password', ['class'=>'span8', 'placeholder' => 'Password'])}}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Password Confirm</label>
            <div class="controls">
                {{ Form::password('password_confirmation', ['class'=>'span8', 'placeholder' => 'Password Confirmation'])}}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

@stop
