@extends('layouts.admin')
@section('title', 'Editing Equipment')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>Edit Health Equipment</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::model($equipment, array('role'=>'form', 'class' => 'form-horizontal row-fluid'))}}
        <div class="control-group">
            <label class="control-label" for="name">Equipment Name</label>
            <div class="controls">
                {{ Form::text('name', $equipment->name, array('class'=>'form-control'))}}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

@stop