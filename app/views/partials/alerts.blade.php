<div class="text-center">
    @if(Session::get('errors'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        @foreach($errors->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ implode('<br />', (array) Session::get('success')) }}
    </div>
    @endif
    @if (Session::has('message'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ implode('<br />', (array) Session::get('message')) }}
    </div>
    @endif
    @if (Session::has('warning'))
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ implode('<br />', (array) Session::get('warning')) }}
    </div>
    @endif
</div>