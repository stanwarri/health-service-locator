<?php

use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration {

    public function up()
    {
        Schema::create('hospitals', function($table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('address');
            $table->string('phone');
            $table->integer('state_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('hospitals');
    }

}