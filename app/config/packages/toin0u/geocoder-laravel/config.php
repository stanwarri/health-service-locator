<?php

/**
 * This file is part of the GeocoderLaravel library.
 *
 * (c) Antoine Corcy <contact@sbin.dk>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return array(
    'providers' => array(
        'Geocoder\Provider\GoogleMapsProvider' => array('', '', $ssl = false, 'AIzaSyCQ9pCX-SD4ZkdiHuFuOJ9Z5RUIM6LvXmc'),
        'Geocoder\Provider\FreeGeoIpProvider'  => null, 
    ),
    'adapter'  => 'Geocoder\HttpAdapter\CurlHttpAdapter'
);
