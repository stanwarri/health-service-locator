@extends('layouts.admin') 
@section('title', 'Create a Story')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>Create Story</h3>
    </div>
    <div class="module-body">
        @include('partials.alerts')
        {{ Form::open(array('route' => 'admin.story.store', 'class' => 'form-horizontal row-fluid', 'files' => true)) }}  
        <div class="control-group">
            <label class="control-label" for="title">Title</label>
            <div class="controls">
                {{ Form::text('title', null, array('class'=>'span8','placeholder'=>'Title')) }}
            </div>
        </div>
        <div class="control-group">
            <textarea class="ckeditor" name="story"></textarea>
        </div>
        <div class="control-group">
            <label class="control-label" for="featured_image">Featured Image</label>
            <div class="controls">
                {{ Form::file('featured_image') }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="video_link">Embed Youtube Video</label>
            <div class="controls">
                {{ Form::text('video_link', null, array('class'=>'span8','placeholder'=>'Embed Youtube Video')) }}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="accident_location">Location of Accident (For Accident Related Stories)</label>
            <div class="controls">
                {{ Form::text('accident_location', null, array('class'=>'span8','placeholder'=>'Accident Location')) }}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" />
@stop

@section('scripts')
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>
<script language="javascript">
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.htmlEncodeOutput = false
</script>

@stop