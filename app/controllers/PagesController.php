<?php

use Illuminate\Support\Facades\View;
use Hsl\Facades\MailService;
use Hsl\Repositories\HospitalRepositoryInterface;
use Hsl\Repositories\CaseRepositoryInterface;
use Hsl\Repositories\ServiceRepositoryInterface;

class PagesController extends BaseController
{

    protected $hospitals;
    protected $cases;
    protected $services;

    public function __construct(HospitalRepositoryInterface $hospitals, CaseRepositoryInterface $cases, ServiceRepositoryInterface $services)
    {
        parent::__construct();

        $this->hospitals = $hospitals;
        $this->cases = $cases;
        $this->services = $services;
    }

    public function getIndex()
    {
        return View::make('pages.index');
    }

    public function getAbout()
    {
        return View::make('pages.about');
    }

    public function getAccidentHotSpot()
    {
        $config = array();
        $config['zoom'] = 'auto';
        $config['styles'] = array(
            array("name" => "Red Parks", "definition" => array(
                    array("featureType" => "all", "stylers" => array(array("saturation" => "-30"))),
                    array("featureType" => "poi.park", "stylers" => array(array("saturation" => "10"), array("hue" => "#990000")))
            )),
            array("name" => "Black Roads", "definition" => array(
                    array("featureType" => "all", "stylers" => array(array("saturation" => "-70"))),
                    array("featureType" => "road.arterial", "elementType" => "geometry", "stylers" => array(array("hue" => "#000000")))
            )),
            array("name" => "No Businesses", "definition" => array(
                    array("featureType" => "poi.business", "elementType" => "labels", "stylers" => array(array("visibility" => "off")))
            ))
        );
        $config['stylesAsMapTypes'] = true;
        $config['stylesAsMapTypesDefault'] = "Black Roads";
        Gmaps::initialize($config);

        $hotspots = DB::table('hotspots')->get();

        foreach ($hotspots as $hotspot) {
            $marker = array();
            $marker['title'] = $hotspot->name;
            $marker['position'] = $hotspot->lat . ',' . $hotspot->lng;
            $marker['clickable'] = TRUE;
            $marker['infowindow_content'] = $hotspot->name . '<br>' . $hotspot->address;
            $marker['icon'] = asset('/assets/img/' . $hotspot->icon);
            Gmaps::add_marker($marker);
        }

        //Create the map
        $map = Gmaps::create_map();

        return View::make('pages.accident-hotspot', compact('map'));
    }

    public function embedEmergencyNumbers()
    {
        $emergencies = Emergency::all();
        return View::make('pages.embed.emergency-numbers', compact('emergencies'));
    }

    //Share Health Story
    public function shareStory()
    {
        return View::make('pages.share-story');
    }

    public function postStory()
    {
        $formData = Input::all();

        $rules = [
            'fullname' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'phone' => 'required',
            'story' => 'required|min:100'
        ];

        $validator = Validator::make($formData, $rules);

        if ($validator->fails()) {
            return Redirect::route('share.story')->withErrors($validator)->withInput($formData);
        }

        $featured_image = NULL;

        if (Input::hasFile('featured_image')) {
            $randomKey = sha1(time() . microtime());
            $extension = Input::file('featured_image')->getClientOriginalExtension();
            $fileName = $randomKey . '.' . $extension;
            $destinationPath = public_path() . '/assets/uploads/';
            // Check if the folder exists on upload, create it if it doesn't
            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777);
            }
            $upload_success = Input::file('featured_image')->move($destinationPath, $fileName);
            if ($upload_success) {
                $featured_image = $fileName;
            }
        }

        $story = Story::create(array(
                    'fullname' => Input::get('fullname'),
                    'email' => Input::get('email'),
                    'address' => Input::get('address'),
                    'phone' => Input::get('phone'),
                    'story' => Input::get('story'),
                    'featured_image' => $featured_image
                ));
        if ($story) {
            MailService::sendAdminHealthStory($story);
            return Redirect::route('share.story')->with('success', 'Thank you for sharing your story with us, we will get back to you soon');
        }
    }

    //Read Full Story
    public function getStory($slug)
    {
        if (is_null($slug)) {
            \App::abort(404);
        }

        $story = Story::where('slug', $slug)->first();

        if (!$story) {
            \App::abort(404);
        }

        $cases = $this->cases->findAll();
        $emergencies = Emergency::all();

        return View::make('pages.story', compact('story', 'cases', 'emergencies'));
    }

    public function getHealthCase($slug)
    {
        if (is_null($slug)) {
            \App::abort(404);
        }

        $case = $this->cases->findBySlug($slug);
        $cases = $this->cases->findAll();
        $emergencies = Emergency::all();

        //Start google maps configuration
        $config = array();
        $config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['cluster'] = TRUE;
        $config['onboundschanged'] = 'if(!centreGot){
        var mapCentre = map . getCenter();
        marker_0.setOptions( {
            position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        Gmaps::initialize($config);

        $hospitals = $this->cases->findHospitalByCase($case);

        foreach ($hospitals as $hospital) {
            $marker = array();
            $marker['title'] = $hospital->name;
            $marker['position'] = $hospital->latitude . ',' . $hospital->longitude;
            $marker['clickable'] = TRUE;
            $marker['infowindow_content'] = $hospital->name . '<br/>' . $hospital->address;
            $marker['icon'] = asset('/assets/img/marker1.png');
            Gmaps::add_marker($marker);
        }

        //Create the map
        $data = array();
        $data['map'] = Gmaps::create_map();

        // dd($hospitals);
        return View::make('pages.case', compact('case', 'hospitals', 'cases', 'emergencies', 'data'));
    }

    public function getHospital($id)
    {
        if (is_null($id)) {
            \App::abort(404);
        }

        $hospital = $this->hospitals->findById($id);
        $cases = $this->cases->findAll();
        $emergencies = Emergency::all();
        return View::make('pages.hospital', compact('hospital', 'cases', 'emergencies'));
    }

    public function feedback()
    {
        return View::make('pages.feedback');
    }

    public function postFeedback()
    {
        $formData = Input::all();

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required|min:100'
        ];

        $validator = Validator::make($formData, $rules);

        if ($validator->fails()) {
            return Redirect::route('feedback')->withErrors($validator)->withInput($formData);
        }

        $feedback = Feedback::create(array(
                    'name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'phone' => Input::get('phone'),
                    'message' => Input::get('message')
                ));
        if ($message) {
            MailService::sendAdminHealthStory($message);
            return Redirect::route('feedback')->with('success', 'Thank you for your feedback');
        }
    }

    /**
     * Show the search results page.
     *
     * @return \Response
     */
    public function getSearch()
    {
        $term = e(Input::get('q'));
        $hospitals = [];
        if (!empty($term)) {
            $hospitals = $this->hospitals->searchByTermPaginated($term, 12);
        }

        $cases = $this->cases->findAll();
        $emergencies = Emergency::all();

        //Start google maps configuration
        $config = array();
        $config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['cluster'] = TRUE;
        $config['onboundschanged'] = 'if(!centreGot){
        var mapCentre = map . getCenter();
        marker_0.setOptions( {
            position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        Gmaps::initialize($config);

        foreach ($hospitals as $hospital) {
            $marker = array();
            $marker['title'] = $hospital->name;
            $marker['position'] = $hospital->latitude . ',' . $hospital->longitude;
            $marker['clickable'] = TRUE;
            $marker['infowindow_content'] = $hospital->name . '<br/>' . $hospital->address;
            $marker['icon'] = asset('/assets/img/marker1.png');
            Gmaps::add_marker($marker);
        }

        //Create the map
        $data = array();
        $data['map'] = Gmaps::create_map();

        return View::make('pages.search', compact('hospitals', 'term', 'cases', 'emergencies', 'data'));
    }

    public function emergencyNumbers()
    {
        $emergencies = Emergency::all();
        return View::make('pages.emergency-numbers', compact('emergencies'));
    }

    public function getStories()
    {
        $stories = Story::where('status', 1)->orderBy('featured', 'desc')->paginate(10);
        $cases = $this->cases->findAll();
        $emergencies = Emergency::all();
        return View::make('pages.stories', compact('stories', 'cases', 'emergencies'));
    }

    public function rateExperience()
    {
        $caseLists = $this->cases->listAll();
        return View::make('pages.rate-experience', compact('caseLists'));
    }

    public function postRateExperience()
    {
        $formData = Input::all();

        $rules = [
            'fullname' => 'required',
            'email' => 'required|email',
            'health_case' => 'required',
            'where_get_help' => 'required'
        ];

        $validator = Validator::make($formData, $rules);

        if ($validator->fails()) {
            return Redirect::route('rate.experience')->withErrors($validator)->withInput($formData);
        }

        $photo = NULL;

        if (Input::hasFile('photo')) {
            $randomKey = sha1(time() . microtime());
            $extension = Input::file('photo')->getClientOriginalExtension();
            $fileName = $randomKey . '.' . $extension;
            $destinationPath = public_path() . '/assets/uploads/';
            // Check if the folder exists on upload, create it if it doesn't
            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777);
            }
            $upload_success = Input::file('photo')->move($destinationPath, $fileName);
            if ($upload_success) {
                $photo = $fileName;
            }
        }

        $survey = Survey::create(array(
                    'fullname' => Input::get('fullname'),
                    'email' => Input::get('email'),
                    'health_case' => Input::get('health_case'),
                    'where_get_help' => Input::get('where_get_help'),
                    'rate_get_help' => Input::get('rate_get_help'),
                    'explain_help' => Input::get('explain_help'),
                    'explain_facility' => Input::get('explain_facility'),
                    'share_story' => Input::get('share_story'),
                    'photo' => $photo
                ));
        if ($survey) {
            //MailService::sendAdminHealthStory($surveory);
            return Redirect::route('rate.experience')->with('success', 'Thank you for rating and sharing your emergency experience');
        }
    }

}
