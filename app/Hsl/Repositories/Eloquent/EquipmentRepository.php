<?php

namespace Hsl\Repositories\Eloquent;

use Equipment;
use Illuminate\Support\Str;
use Hsl\Services\Forms\EquipmentCreateForm;
use Hsl\Repositories\EquipmentRepositoryInterface;

class EquipmentRepository extends AbstractRepository implements EquipmentRepositoryInterface
{

    /**
     * Create a new DbTagRepository instance.
     *
     * @param  Equipment $equipments
     * @return void
     */
    public function __construct(Equipment $equipment)
    {
        $this->model = $equipment;
    }

    /**
     * Get an array of key-value (id => name) pairs of all equipment.
     *
     * @return array
     */
    public function listAll()
    {
        $equipment = $this->model->lists('name', 'id');

        return $equipment;
    }

    /**
     * Find all equipments.
     *
     * @param  string  $orderColumn
     * @param  string  $orderDir
     * @return \Illuminate\Database\Eloquent\Collection|Equipment[]
     */
    public function findAll($orderColumn = 'name', $orderDir = 'asc')
    {
        $equipment = $this->model

                ->orderBy($orderColumn, $orderDir)
                ->get();
        return $equipment;
    }

    //Find equipment by ids
    public function getEquipmentByIds($ids)
    {
        if (!$ids) return null;
        return $this->model->whereIn('id', $ids)->get();
    }
    /**
     * Find a equipment by id.
     *
     * @param  mixed  $id
     * @return Equipment
     */
    public function findById($id)
    {
        if (!$id) return null;
        return $this->model->find($id);
    }

    /**
     * Create a new equipment in the database.
     *
     * @param  array  $data
     * @return Equipment
     */
    public function create(array $data)
    {
        $equipment = $this->getNew();

        $equipment->name = $data['name'];
        $equipment->slug = Str::slug($equipment->name, '-');

        $equipment->save();

        return $equipment;
    }

    /**
     * Update the specified equipment in the database.
     *
     * @param  mixed  $id
     * @param  array  $data
     */
    public function update($id, array $data)
    {
        $equipment = $this->findById($id);

        $equipment->name = $data['name'];
        $equipment->slug = Str::slug($equipment->name, '-');

        $equipment->save();

        return $equipment;
    }

    /**
     * Delete the specified equipment from the database.
     *
     * @param  mixed  $id
     * @return void
     */
    public function delete($id)
    {
        $equipment = $this->findById($id);
        $equipment->delete();
    }

    /**
     * Get the equipment create/update form equipment.
     *
     * @return \Codulab\Form\CreateTagForm
     */
    public function getCreateEquipmentForm()
    {
        return new EquipmentCreateForm;
    }
}
