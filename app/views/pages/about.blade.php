@extends('layouts.main')
@section('title', 'About us')

@section('content')     
<section class="content content-light" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-center header"><strong>The Collective</strong></p>
            </div>
        </div>
        <hr class="style-1" style="width:18%">
        <div class="row">
            <div class="col-md-5 col-sm-6 col-md-offset-1 text-right about-info">
                <h2>who are we?</h2>
                <p>We are a multidisciplinary team of strategists, designers, storytellers, & creative coders. </p>
            </div>
            <div class="col-md-5 col-sm-6 text-left about-info">
                <h2>
                    what do we do?
                </h2>
                <p>We build web/mobile apps that are commercially viable and deliver a valuable user experience</p>
            </div>
        </div>
    </div>
</section>

<section class="content content-dark bg-about-join-team-img">
    <div class="container">
        <p class="header text-center text-white">Let's <strong>Do</strong> Something Great</p>
        <p class="header header-tiny text-center text-white">We’re ready to build businesses with interesting, passionate idea people from all over the world.</p>
        <p class="buttons text-center"><a href="http://codulab.com/team/netaviva/jobs" class="btn btn-theme btn-orange">Join The Team</a></p>
    </div>
</section>

<section class="content content-light">
    <div class="container">
        <p class="text-center header">Our <strong>Technology Stacks</strong></p>
        <p class="text-center">At Netaviva, We build using top and modern technology stacks. We put in best practices into our work.</p>

        <hr class="invisible" />
        <hr class="invisible" />

        <!-- Technology Stacks -->
        <div class="row text-center">
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-java-line"></i></div>			
                    <h3>Java</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="icon-nodejs"></i></div>
                    <h3>NodeJS</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-laravel-plain"></i></div>
                    <h3>Laravel</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-php-plain"></i></div>
                    <h3>PHP</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-python-plain"></i></div>			
                    <h3>Python</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-mysql-line"></i></div>
                    <h3>MySQL</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-angularjs-plain"></i></div>
                    <h3>AngularJS</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-bootstrap-plain"></i></div>
                    <h3>Twitter Bootstrap</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-bower-line"></i></div>			
                    <h3>Bower</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-git-plain"></i></div>
                    <h3>Git</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-html5-plain"></i></div>
                    <h3>HTML5</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-css3-plain"></i></div>
                    <h3>CSS3</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-jquery-plain"></i></div>			
                    <h3>jQuery</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="devicon-sass-plain"></i></div>
                    <h3>SASS</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="icon-nginx-alt"></i></div>
                    <h3>Nginx</h3>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="icon-redis"></i></div>
                    <h3>Redis</h3>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/font-mfizz.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/devicon.css') }}" />
@stop