<?php

namespace Admin;

use Story;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class StoryController extends \BaseController
{

    /**
     * Show the services index page.
     *
     * @return \Response
     */
    public function index()
    {
        $stories = Story::paginate(10);

        return View::make('admin.story.index', compact('stories'));
    }

    public function create()
    {
        return View::make('admin.story.create');
    }

    /**
     * Handle the creation of a service.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $formData = Input::all();

        $rules = [
            'title' => 'required',
            'story' => 'required|min:100'
        ];

        $validator = Validator::make($formData, $rules);

        if ($validator->fails()) {
            return $this->redirectRoute('admin.story.new')->withErrors($validator)->withInput($formData);
        }

        $featured_image = NULL;

        if (Input::hasFile('featured_image')) {
            $randomKey = sha1(time() . microtime());
            $extension = Input::file('featured_image')->getClientOriginalExtension();
            $fileName = $randomKey . '.' . $extension;
            $destinationPath = public_path() . '/assets/uploads/';
            // Check if the folder exists on upload, create it if it doesn't
            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777);
            }
            $upload_success = Input::file('featured_image')->move($destinationPath, $fileName);
            if ($upload_success) {
                $featured_image = $fileName;
            }
        }

        $story = Story::create(array(
                    'fullname' => Auth::user()->fullname,
                    'email' => Auth::user()->email,
                    'title' => Input::get('title'),
                    'slug' => Str::slug(Input::get('title'), '-'),
                    'story' => Input::get('story'),
                    'featured_image' => $featured_image,
                    'video_link' => Input::get('video_link'),
                    'status' => 1
                ));
        if ($story) {
            return $this->redirectRoute('admin.story.index')->with('success', 'Story successfully shared');
        }
    }

    /**
     * Show the service edit form.
     *
     * @param  mixed $id
     * @return \Response
     */
    public function edit($id)
    {

        $story = Story::find($id);
        return View::make('admin.story.edit', compact('story'));
    }

    /**
     * Handle the editting of a service.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $formData = Input::all();

        $rules = [
            'title' => 'required',
            'story' => 'required|min:100'
        ];

        $validator = Validator::make($formData, $rules);

        if ($validator->fails()) {
            return $this->redirectRoute('admin.story.edit', $id)->withErrors($validator)->withInput($formData);
        }

        $story = Story::find($id);
        
        $featured_image = $story->featured_image;

        if (Input::hasFile('featured_image')) {
            $randomKey = sha1(time() . microtime());
            $extension = Input::file('featured_image')->getClientOriginalExtension();
            $fileName = $randomKey . '.' . $extension;
            $destinationPath = public_path() . '/assets/uploads/';
            // Check if the folder exists on upload, create it if it doesn't
            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777);
            }
            $upload_success = Input::file('featured_image')->move($destinationPath, $fileName);
            if ($upload_success) {
                if ($story->featured_image) {
                    File::delete($destinationPath . $story->featured_image);
                }
                $featured_image = $fileName;
            }
        }

        $story->title = Input::get('title');
        $story->slug = Str::slug(Input::get('title'), '-');
        $story->story = Input::get('story');
        $story->featured_image = $featured_image;
        $story->status = 1;
        $story->featured = 0;


        $story->save();

        if ($story) {
            return $this->redirectRoute('admin.story.index');
        }
    }

    /**
     * Delete a service from the database.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $story = Story::find($id);
        $story->delete();
        return $this->redirectRoute('admin.story.index');
    }

}
