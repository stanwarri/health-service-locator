<!-- Start Header -->
<header id="header">
    <div class="header-inner">
        <!-- Start Login-Shadow -->
        <div id="login-shadow"></div>
        <!-- End Login-Shadow -->
        <div class="container">
            <!-- Start Menu Nav -->
            <div class="menu-nav row">
                <!-- Start Logo -->
                <div class="logo col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a href="{{ route('index') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="logo"></a>
                </div>
                <!-- End Logo -->
                <!-- Start Nav -->
                <nav id="nav-wrapper" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <ul class="nav  no-squares custom-list pull-right">
                        <li>
                            <a href="{{ route('index') }}">
                                <span class="caption">Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('accident.hotspot') }}">
                                <span class="caption">Accident Hotspots</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('share.story') }}">
                                <span class="caption">Share Your Story</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('feedback') }}">
                                <span class="caption">Feedback</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Nav -->

                <!-- End Search Nav Mobile -->
                <!-- Start Nav-Wrapper Mobile -->
                <nav class="nav-wrapper-mobile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="main-menu custom-list">
                        <li>
                            <a href="{{ route('index') }}">
                                <span class="caption">Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('accident.hotspot') }}">
                                <span class="caption">Accident Hotspots</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('share.story') }}">
                                <span class="caption">Share Your Story</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('feedback') }}">
                                <span class="caption">Feedback</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Nav-Wrapper Mobile -->
            </div>
            <!-- End Menu Nav -->
            <!-- Responsive Menu Buttons -->
            <button class="navbar-toggle button"><i class="fa fa-bars"></i></button>
            <!-- End Responsive Menu Buttons -->
        </div>
    </div>
</header>
<!-- End Header -->