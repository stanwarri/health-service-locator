@extends('layouts.main')
@section('title', 'Welcome')
@section("body_onload", "")@stop

@section('content')
<!--banner starts here-->
<section class="home-top-slider">
    <div class="color-overlay"></div>
    <div class="home-top">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box-caption">
                        <h1 class="intro">When you or your loved one faces a life-threatening emergency, what do you do?</h1>
                        <div class="text-center intro-button">
                            <a href="{{ route('health.locator') }}" class="btn btn-success btn-large"><i class="fa fa-hospital-o"></i>Find Nearest Hospital</a>
                        </div>
                    </div>
                </div>
                <div class="call-to-action-box col-md-4 pull-right">
                    <ul class="list-unstyled">
                        <li class="btn btn-warning btn-large home-btn-custom"><i class="fa fa-phone"></i><a href="{{ route('emergency.numbers') }}"> Find Emergency Numbers</a></li>
                        <li class="btn btn-warning btn-large home-btn-custom"><i class="fa fa-star"></i><a href="{{ route('rate.experience') }}"> Rate Your Experience</a></li>
                        <li class="btn btn-warning btn-large home-btn-custom"><i class="fa fa-newspaper-o"></i><a href="{{ route('stories') }}"> Read Health Stories</a></li>
                    </ul><!--//list-unstyled-->
                </div>
            </div>
        </div>
    </div> 
</section>

<div id="main-content" style="margin-bottom: 30px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 style="text-align:center;padding-top: 20px">Inspiring Health Stories</h3>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 20px"></div>

    <div class="container">
        <div class="row">
            @foreach($stories as $story)
            <div class="col-lg-6">
                <div class="overlay">
                    <img src="{{ asset('assets/uploads/'.$story->featured_image.'') }}" alt="">
                    <div class="overlay-shadow">
                        <div class="overlay-content">
                            <a href="{{ route('read.story', $story->slug) }}" class="btn"><i class="fa fa-arrow-circle-right"></i>Read Story</a>
                        </div>
                    </div>
                </div>
                <div id="blog" style="padding-bottom: 40px">
                    <div class="blog-list-post">
                        <div class="blog-list-header clearfix">
                            <h5 class="title"><a href="{{ route('read.story', $story->slug) }}">{{ $story->title }}</a></h5>
                        </div>
                        <div class="blog-list-content clearfix">
                            <p>{{ str_limit(strip_tags($story->story), 154) }}</p>
                        </div>
                        <div class="blog-list-meta">
                            <span class="date">
                                <i class="fa fa-calendar"></i>{{ $story->created_at->format('l, jS F Y h:i:s A') }}
                            </span>
                            <span class="button">
                                <a href="{{ route('read.story', $story->slug) }}" class="btn btn-default btn-sm"><i class="fa fa-arrow-circle-right"></i>Read Story</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="text-center">
                <a href="{{ route('stories') }}" class="btn btn-info btn-large"><i class="fa fa-newspaper-o"></i>See More Health Stories</a>
            </div>
        </div>
    </div>
</div>

@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/slider.css') }}">
@stop

@section('scripts')
<script src="{{ asset('assets/js/jquery.bgswitcher.js') }}"></script>
<script>   
    $(".home-top").bgswitcher({
        images: ["{{ asset('assets/img/home-slide-1.jpg') }}", "{{ asset('assets/img/home-slide-4.jpg') }}", "{{ asset('assets/img/home-slide-3.jpg') }}"], // Background images
        effect: "fade", // fade, blind, clip, slide, drop, hide
        interval: 8000, // Interval of switching
        loop: true, // Loop the switching
        shuffle: false // Shuffle the order of an images
    });
    //    $(".home-top").backstretch([
    //        "{{ asset('assets/img/lagos-emergency-point.jpg') }}",
    //        "{{ asset('assets/img/crash.jpg') }}"
    //    ], {
    //        fade: 750,
    //        duration: 4000
    //    });
</script>
@stop
