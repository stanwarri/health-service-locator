<?php

# Admin routes
# Route filters
Route::when('admincp/*', 'admincp');
/**
 * Admin Panel routes
 */
Route::group(['prefix' => '/admincp', 'before' => 'auth'], function () {
            Route::match(['GET'], '', [
                'as' => 'admin.dashboard',
                'uses' => 'Admin\DashboardController@dashboard'
            ]);

            Route::match(['GET'], 'hospitals', [
                'as' => 'admin.hospitals.index',
                'uses' => 'Admin\HospitalsController@index'
            ]);

            Route::match(['GET'], 'stories', [
                'as' => 'admin.story.index',
                'uses' => 'Admin\StoryController@index'
            ]);


            Route::match(['GET'], 'story/new', [
                'as' => 'admin.story.new',
                'uses' => 'Admin\StoryController@create'
            ]);

            Route::match(['POST'], 'story/store', [
                'as' => 'admin.story.store',
                'uses' => 'Admin\StoryController@store'
            ]);

            Route::match(['GET'], 'story/{id}/edit', [
                'as' => 'admin.story.edit',
                'uses' => 'Admin\StoryController@edit'
            ]);


            Route::match(['POST'], 'story/{id}/edit', [
                'uses' => 'Admin\StoryController@update'
            ]);

            Route::match(['GET'], '/story/delete/{id?}', [
                'as' => 'admin.story.delete',
                'uses' => 'Admin\StoryController@delete'
            ]);


            Route::match(['GET'], 'hospitals/new', [
                'as' => 'admin.hospitals.new',
                'uses' => 'Admin\HospitalsController@create'
            ]);

            Route::match(['POST'], 'hospitals/new', [
                'uses' => 'Admin\HospitalsController@store'
            ]);

            Route::match(['GET'], 'hospitals/{id}/edit', [
                'as' => 'admin.hospitals.edit',
                'uses' => 'Admin\HospitalsController@edit'
            ]);

            Route::match(['POST'], 'hospitals/{id}/edit', [
                'uses' => 'Admin\HospitalsController@update'
            ]);

            Route::match(['GET'], 'hospitals/delete/{id?}', [
                'as' => 'admin.hospitals.delete',
                'uses' => 'Admin\HospitalsController@delete'
            ]);

            Route::match(['GET'], '/users', [
                'as' => 'admin.users.index',
                'uses' => 'Admin\UserController@index'
            ]);

            Route::match(['GET'], '/user/create', [
                'as' => 'admin.users.create',
                'uses' => 'Admin\UserController@create'
            ]);

            Route::match(['POST'], '/user/create', [
                'uses' => 'Admin\UserController@store'
            ]);

            Route::get('/user/account', [
                'as' => 'admin.users.account.edit',
                'uses' => 'Admin\UserController@getEditAccount'
            ]);

            Route::post('/user/account', [
                'uses' => 'Admin\UserController@postEditAccount'
            ]);

           Route::get('user/edit/{id}', [
                'as' => 'admin.users.edit',
                'uses' => 'Admin\UserController@editUser'
            ]);

            Route::post('user/edit/{id}', [
                'uses' => 'Admin\UserController@postEditUser'
            ]);

            Route::match(['GET'], '/users/delete/{id?}', [
                'as' => 'admin.users.delete',
                'uses' => 'Admin\UserController@delete'
            ]);


            Route::match(['GET'], '/emergency/numbers', [
                'as' => 'admin.emergency.index',
                'uses' => 'Admin\EmergencyController@index'
            ]);

            Route::match(['GET'], '/emergency/number/new', [
                'as' => 'admin.emergency.new',
                'uses' => 'Admin\EmergencyController@create'
            ]);

            Route::match(['POST'], '/emergency/number/new', [
                'uses' => 'Admin\EmergencyController@store'
            ]);

            Route::match(['GET'], '/services', [
                'as' => 'admin.services.index',
                'uses' => 'Admin\ServicesController@getIndex'
            ]);

            Route::match(['POST'], 'services/new', [
                'as' => 'admin.services.new',
                'uses' => 'Admin\ServicesController@store'
            ]);

            Route::match(['GET'], '/services/edit/{id}', [
                'as' => 'admin.services.edit',
                'uses' => 'Admin\ServicesController@edit'
            ]);

            Route::match(['POST'], '/services/edit/{id}', [
                'uses' => 'Admin\ServicesController@update'
            ]);

            Route::match(['GET'], '/services/delete/{id?}', [
                'as' => 'admin.services.delete',
                'uses' => 'Admin\ServicesController@getDelete'
            ]);

            Route::match(['GET'], '/equipment', [
                'as' => 'admin.equipment.index',
                'uses' => 'Admin\EquipmentController@getIndex'
            ]);

            Route::match(['POST'], 'equipment/new', [
                'as' => 'admin.equipment.new',
                'uses' => 'Admin\EquipmentController@store'
            ]);

            Route::match(['GET'], '/equipment/edit/{id}', [
                'as' => 'admin.equipment.edit',
                'uses' => 'Admin\EquipmentController@edit'
            ]);

            Route::match(['POST'], '/equipment/edit/{id}', [
                'uses' => 'Admin\EquipmentController@update'
            ]);

            Route::match(['GET'], '/equipment/delete/{id?}', [
                'as' => 'admin.equipment.delete',
                'uses' => 'Admin\EquipmentController@getDelete'
            ]);

            Route::match(['GET'], '/cases', [
                'as' => 'admin.cases.index',
                'uses' => 'Admin\CasesController@index'
            ]);

            Route::match(['POST'], 'cases/new', [
                'as' => 'admin.cases.new',
                'uses' => 'Admin\CasesController@store'
            ]);

            Route::match(['GET'], '/cases/edit/{id}', [
                'as' => 'admin.cases.edit',
                'uses' => 'Admin\CasesController@edit'
            ]);

            Route::match(['POST'], '/cases/edit/{id}', [
                'uses' => 'Admin\CasesController@update'
            ]);

            Route::match(['GET'], '/cases/delete/{id?}', [
                'as' => 'admin.cases.delete',
                'uses' => 'Admin\CasesController@delete'
            ]);
        });



